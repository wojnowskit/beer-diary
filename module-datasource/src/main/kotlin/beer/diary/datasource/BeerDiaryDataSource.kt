package beer.diary.datasource

import org.postgresql.ds.PGSimpleDataSource
import javax.sql.DataSource

object BeerDiaryDataSource {

    lateinit var dataSource:DataSource

    fun buildDataSource(params: BeerDiaryDbParams) {
        val pgDataSource = PGSimpleDataSource()
        pgDataSource.user = params.user
        pgDataSource.password = params.password
        pgDataSource.databaseName = params.dbName
        pgDataSource.serverName = params.host
        pgDataSource.portNumber = params.port
        this.dataSource = pgDataSource
    }

}