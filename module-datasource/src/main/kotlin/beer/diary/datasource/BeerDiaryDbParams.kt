package beer.diary.datasource

data class BeerDiaryDbParams(
        val host: String,
        val dbName: String,
        val user: String,
        val password: String,
        val port: Int
)