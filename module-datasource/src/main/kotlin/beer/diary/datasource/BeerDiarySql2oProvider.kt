package beer.diary.datasource

import org.sql2o.Sql2o
import javax.sql.DataSource

object BeerDiarySql2oProvider {

    lateinit var sql2o:Sql2o

    fun buildSql2o(dataSource: DataSource) {
        sql2o = Sql2o(dataSource)
    }

}