package beer.diary.datasource

import org.sql2o.Sql2o
import javax.sql.DataSource

const val BEER_SCHEMA = "beer_diary"

interface BeerDiaryDbConfiguration {

    fun dataSource(): DataSource
    fun sql2o(): Sql2o

}

class DefaultBeerDiaryDbConfiguration(beerDiaryDbParams: BeerDiaryDbParams) : BeerDiaryDbConfiguration {

    init {
        BeerDiaryDataSource.buildDataSource(beerDiaryDbParams)
        BeerDiarySql2oProvider.buildSql2o(BeerDiaryDataSource.dataSource)
    }

    override fun dataSource(): DataSource {
        return BeerDiaryDataSource.dataSource
    }

    override fun sql2o(): Sql2o {
        return BeerDiarySql2oProvider.sql2o
    }

}

class UnitTestBeerDiaryDbConfiguration : BeerDiaryDbConfiguration {

    init {
        BeerDiaryDataSource.buildDataSource(testParams())
        BeerDiarySql2oProvider.buildSql2o(BeerDiaryDataSource.dataSource)
    }

    private fun testParams() = BeerDiaryDbParams(
            "localhost",
            "test-db",
            "dummyuser",
            "dummypass",
            15432
    )

    override fun dataSource(): DataSource {
        return BeerDiaryDataSource.dataSource
    }

    override fun sql2o(): Sql2o {
        return BeerDiarySql2oProvider.sql2o
    }

}
