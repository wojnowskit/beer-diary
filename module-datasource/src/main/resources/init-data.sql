-- test-pass
-- $2a$10$/qd/EHzSusKk1XmkK67wzOoLPLiJUAwiu.XRM1ZcOCHMpZsOqzqQy
INSERT INTO beer_diary.app_users(user_name, user_password) VALUES('test-user', '$2a$10$/qd/EHzSusKk1XmkK67wzOoLPLiJUAwiu.XRM1ZcOCHMpZsOqzqQy');