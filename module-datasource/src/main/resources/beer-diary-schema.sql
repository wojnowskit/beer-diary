CREATE SCHEMA beer_diary;

CREATE TABLE beer_diary.app_users(
    user_id SERIAL PRIMARY KEY,
    user_name TEXT NOT NULL UNIQUE,
    user_password TEXT NOT NULL
);

CREATE TABLE beer_diary.beers(
    beer_id SERIAL PRIMARY KEY,
    beer_name TEXT NOT NULL,
    beer_description TEXT NOT NULL,
    beer_created_at TIMESTAMP NOT NULL,
    beer_created_by INTEGER NOT NULL
);

CREATE TABLE beer_diary.beer_notes(
    note_id SERIAL PRIMARY KEY,
    note_description TEXT NOT NULL,
    note_created_at TIMESTAMP NOT NULL,
    note_beer_id INTEGER REFERENCES beer_diary.beers (beer_id)
);

CREATE TABLE beer_diary.ingredients_dict(
    ingredient_id SERIAL PRIMARY KEY,
    ingredient_name TEXT NOT NULL UNIQUE
);

CREATE TABLE beer_diary.beer_ingredients(
    beer_ingredient_id SERIAL PRIMARY KEY,
    beer_ingredient_info TEXT NOT NULL,
    beer_ingredient_beer_id INTEGER REFERENCES beer_diary.beers (beer_id),
    beer_ingredient_dict_id INTEGER REFERENCES beer_diary.ingredients_dict (ingredient_id)
);