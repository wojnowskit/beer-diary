import Vue from 'vue'
import ElementUI from 'element-ui'
import vueMoment from 'vue-moment'
import VueClipboard from 'vue-clipboard2'

import App from './App'

Vue.config.productionTip = false

Vue.use(vueMoment)
Vue.use(ElementUI, {})
Vue.use(VueClipboard)

new Vue(
 {
   components: {App},
   template: '<App/>'
 }
).$mount('#app')
