/* eslint-ignore */

module.exports = {
 root: true,
 env: {
   node: true
 },
 'extends': [
 ],
 rules: {
 },
 overrides: [
 ],
 parserOptions: {
   parser: 'babel-eslint'
 },
 globals: {
   jest: true,
   describe: true,
   expect: true,
   it: true,
   beforeEach: true
 }
}
