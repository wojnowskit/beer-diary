package beer.diary.web

import beer.diary.conf.BeerApiConfiguration
import beer.diary.datasource.BeerDiaryDbConfiguration
import beer.diary.search.elastic.config.ElasticConfig
import beer.diary.search.elastic.config.ElasticSearchConfiguration
import beer.diary.web.security.SecurityConfiguration
import io.javalin.Javalin

const val API_BASE_PATH = "api"

fun registerApp(originConfig: OriginConfig, dbConfiguration: BeerDiaryDbConfiguration, elasticConfig: ElasticConfig): Javalin {
    val app = Javalin.create {
        it.enableCorsForOrigin(*originConfig.origins())
    }
    val securityConfiguration = SecurityConfiguration(dbConfiguration.sql2o())
    val searchConfiguration = ElasticSearchConfiguration(elasticConfig)
    val eventsConsumer = searchConfiguration.eventsConsumer();
    val beerSearchExecutor = searchConfiguration.searchExecutor()
    val searchIndexManager = searchConfiguration.searchIndexManager()
    searchIndexManager.initIndexIfNotExists()
    val beerApi = BeerApiConfiguration(dbConfiguration, eventsConsumer).beerApi()
    val securityHandler = securityConfiguration.buildSecurityHandler()
    val loginExecutor = securityConfiguration.buildLoginExecutor()

    BeerWebSecurityConfiguration(app, loginExecutor, securityHandler).register()
    BeerWebSearchConfiguration(app, beerSearchExecutor).registerController()
    BeerWebConfiguration(app, beerApi).registerController()
    return app
}

data class OriginConfig(
        private val allowedOrigins: List<String>
) {
    fun origins() = allowedOrigins.toTypedArray()
}