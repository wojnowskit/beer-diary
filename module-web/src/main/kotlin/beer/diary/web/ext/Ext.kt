package beer.diary.web.ext

import beer.diary.commons.exception.BaseException
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.javalin.http.Context
import io.vavr.control.Either
import java.util.*

val mapper = ObjectMapper()

fun Map<String,Any>.toTextField(paramName:String): TextField {
    val value = get(paramName) ?: ""
    return TextField(
            paramName,
            value.toString()
    )
}

fun Map<String,Any>.beerId(): PositiveNumber {
    val value = get("beerId") ?: "-1"
    return PositiveNumber(
            value.toString().toLong()
    )
}

fun Map<String,Any>.getNumber(key: String): PositiveNumber {
    val value = get(key) ?: "0"
    return PositiveNumber(
            value.toString().toLong()
    )
}

@Suppress("UNCHECKED_CAST")
fun <T> Map<String, Any>.createItems(itemsKey: String, producer: (Map<String,String>) -> T): List<T> {
    return Optional.ofNullable(get(itemsKey))
            .map { it as ArrayList<Any> }
            .map { it ->
                return@map it
                        .map { it as LinkedHashMap<String, String> }
                        .map { producer(it) }
            }.orElse(emptyList())
}

fun Context.bodyParams() = mapper.readValue(body()) as Map<String,Any>

fun <L : BaseException, R : Any> Either<L, R>.handleJson(ctx: Context) {
    if (isRight) {
        ctx.json(get())
    } else {
        ctx.status(left.errorCode())
        ctx.json(left.info())
    }
}