package beer.diary.web.ext

import beer.diary.api.beer.*
import beer.diary.api.beer.ingredient.BeerIngredient
import beer.diary.api.beer.ingredient.NewBeerIngredient
import beer.diary.api.beer.note.BeerNote
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.search.api.PaginateCommand
import beer.diary.search.api.SearchCommand
import beer.diary.search.api.SearchData
import beer.diary.search.api.fields.SearchTextField
import beer.diary.web.security.LoginCommand
import io.javalin.http.Context


fun Map<String, Any>.createNewNotes() = createItems("newNotes") {
    NewBeerNote(
            it.toTextField("description"),
            DiaryDate.ofNow()
    )
}

fun Map<String, Any>.createNotes() = createItems("notes") {
    BeerNote(
            it.getNumber("id"),
            it.toTextField("description")
    )
}

fun Map<String, Any>.createNewIngredients() = createItems("newIngredients") {
    NewBeerIngredient(
            it.toTextField("name"),
            it.toTextField("info")
    )
}

fun Map<String, Any>.createIngredients() = createItems("ingredients") {
    BeerIngredient(
            it.getNumber("id"),
            it.toTextField("name"),
            it.toTextField("info")
    )
}

fun Context.toCreateBeerCommand(): CreateBeerCommand {
    val params = bodyParams()
    return CreateBeerCommand(
            params.toTextField("name"),
            params.toTextField("description"),
            getSecurityUser().asBeerOwnerId(),
            DiaryDate.ofNow(),
            params.createNewNotes(),
            params.createNewIngredients()
    )
}

fun Context.toUpdateBeerCommand() : UpdateBeerCommand {
    val params = bodyParams()
    return UpdateBeerCommand(
            params.beerId(),
            params.toTextField("name"),
            params.toTextField("description"),
            getSecurityUser().asBeerOwnerId(),
            params.createNewNotes(),
            params.createNotes(),
            params.createNewIngredients(),
            params.createIngredients()
    )
}

fun Context.toBeerReadCommand(): BeerReadCommand {
    val beerId = pathParam("beerId").toLong()
    return BeerReadCommand(
            PositiveNumber(beerId),
            getSecurityUser().asBeerOwnerId()
    )
}

fun Context.toLoginCommand(): LoginCommand {
    val params = bodyParams()
    return LoginCommand(
            params.toTextField("username"),
            params.toTextField("password")
    )
}

fun Context.toBeerDeleteCommand(): BeerDeleteCommand {
    val beerId = pathParam("beerId").toLong()
    return BeerDeleteCommand(
            PositiveNumber(beerId),
            getSecurityUser().asBeerOwnerId()
    )
}

fun Context.toSearchCommand(): SearchCommand {
    val params = bodyParams()
    val searchText = params["searchText"] ?: ""
    return SearchCommand(
            SearchTextField(searchText as String),
            SearchData(
                    PositiveNumber(
                            getSecurityUser().id
                    )
            ),
            PaginateCommand(
                    params.getNumber("page"),
                    params.getNumber("itemsPerPage")
            )
    )
}