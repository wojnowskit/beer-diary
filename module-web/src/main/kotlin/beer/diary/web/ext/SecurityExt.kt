package beer.diary.web.ext

import beer.diary.api.beer.BeerOwnerId
import beer.diary.commons.fields.PositiveNumber
import beer.diary.web.security.SECURITY_USER_KEY
import beer.diary.web.security.SecurityUser
import io.javalin.http.Context

fun Context.getSecurityUser(): SecurityUser {
    return attribute<SecurityUser>(SECURITY_USER_KEY)!!
}

fun SecurityUser.asBeerOwnerId() : BeerOwnerId {
    return BeerOwnerId(
            PositiveNumber(id)
    )
}