package beer.diary.web

import beer.diary.web.ext.handleJson
import beer.diary.web.ext.toLoginCommand
import beer.diary.web.security.LoginCommand
import beer.diary.web.security.LoginExecutor
import beer.diary.web.security.SecurityHandler
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import io.javalin.http.Context

class BeerWebSecurityConfiguration(
        private val app: Javalin,
        private val loginExecutor: LoginExecutor,
        private val securityHandler: SecurityHandler
) {

    private fun registerSecurity() {
        app.before("/$API_BASE_PATH/*", securityHandler)
    }

    private fun registerController() {
        val controller = BeerLoginController(loginExecutor)
        app.routes {
            ApiBuilder.path("login") {
                ApiBuilder.post("/", controller::login)
            }
        }
    }

    fun register() {
        registerController()
        registerSecurity()
    }

}

private class BeerLoginController(private val loginExecutor: LoginExecutor) {

    fun login(ctx: Context) {
        val command: LoginCommand = ctx.toLoginCommand()
        loginExecutor.execute(command)
                .handleJson(ctx)
    }

}
