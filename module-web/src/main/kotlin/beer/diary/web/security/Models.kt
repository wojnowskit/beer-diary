package beer.diary.web.security

import beer.diary.commons.exception.BaseException
import beer.diary.commons.fields.TextField
import beer.diary.commons.validation.ErrorCode
import beer.diary.commons.validation.ValidData
import beer.diary.commons.validation.ValidationResult
import beer.diary.commons.validation.ValidationResultFactory.ofMultiple
import java.util.*

const val SECURITY_USER_KEY = "suk"

data class SecurityUser(
        val id: Long,
        val name: String
)

data class LoginCommand(
        val username: TextField,
        val password: TextField
) : ValidData {
    override fun validate(): ValidationResult {
        return ofMultiple(
                listOf(username, password)
        )
    }

    fun toBasicAuthHeader() : String {
        val hash = Base64.getEncoder()
                .encodeToString("${username.value()}:${password.value()}".toByteArray())
        return "Basic $hash"
    }

}

data class LoginResult(
        val hash: String
)

class LoginException(errors: List<ErrorCode>) : BaseException(errors)