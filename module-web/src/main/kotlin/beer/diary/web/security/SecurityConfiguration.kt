package beer.diary.web.security

import org.sql2o.Sql2o

class SecurityConfiguration(private val sql2o: Sql2o) {

    private val securityUserRepository: SecurityUserRepository = SimpleSecurityUserRepository(sql2o)

    fun buildSecurityHandler(): SecurityHandler {
        return SecurityHandler(
                securityUserRepository
        )
    }

    fun buildLoginExecutor() : LoginExecutor {
        return SimpleLoginExecutor(
                securityUserRepository
        )
    }

}