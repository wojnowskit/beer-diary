package beer.diary.web.security

import io.javalin.http.Context
import io.javalin.http.Handler
import io.javalin.http.UnauthorizedResponse


class SecurityHandler(private val securityUserRepository: SecurityUserRepository) : Handler {

    override fun handle(ctx: Context) {
        if (ctx.basicAuthCredentialsExist()) {
            handleCredentials(ctx)
        } else {
            unAuth(ctx)
        }
    }

    private fun handleCredentials(ctx: Context) {
        val credentials = ctx.basicAuthCredentials()
        securityUserRepository.findUser(credentials.username, credentials.password)
                .ifPresentOrElse({
                    ctx.attribute(SECURITY_USER_KEY, it)
                },{
                  unAuth(ctx)
                })
    }

    private fun unAuth(ctx: Context) {
        ctx.status(401).json("Unauthorized-request-sorry")
        throw UnauthorizedResponse()
    }

}