package beer.diary.web.security

import beer.diary.commons.executors.MultiValidateExecutor
import beer.diary.commons.validation.ErrorCode
import io.vavr.control.Either

abstract class LoginExecutor : MultiValidateExecutor<LoginCommand, LoginResult, LoginException>()

class SimpleLoginExecutor(private val securityUserRepository: SecurityUserRepository) : LoginExecutor() {

    override fun executeValidated(command: LoginCommand): Either<LoginException, LoginResult> {
        return securityUserRepository.findUser(
                command.username.value(),
                command.password.value()
        ).map {
            Either.right<LoginException, LoginResult>(LoginResult(command.toBasicAuthHeader()))
        }.orElse(credentialsException())
    }

    private fun credentialsException() : Either<LoginException, LoginResult> {
        return Either.left(LoginException(listOf(
            ErrorCode("credentials", "credentials.data.error")
        )))
    }

}