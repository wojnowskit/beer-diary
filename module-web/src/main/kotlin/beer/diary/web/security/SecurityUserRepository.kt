package beer.diary.web.security

import beer.diary.datasource.BEER_SCHEMA
import org.mindrot.jbcrypt.BCrypt
import org.sql2o.Sql2o
import java.util.*


interface SecurityUserRepository {
    fun findUser(username: String, rawPassword: String): Optional<SecurityUser>
}

class SimpleSecurityUserRepository(private val sql2o: Sql2o) : SecurityUserRepository {

    override fun findUser(username: String, rawPassword: String): Optional<SecurityUser> {
        return optionalUserData(username)
                .filter {
                    BCrypt.checkpw(rawPassword, it.password)
                }
                .map {
                    SecurityUser(
                            it.id,
                            username
                    )
                }
    }

    private fun optionalUserData(username: String): Optional<UserData> {
        val value = sql2o.open().use {
            return@use it.createQuery("""
                SELECT user_id AS id, user_password AS password
                FROM ${BEER_SCHEMA}.app_users 
                WHERE user_name = :username LIMIT 1
            """.trimIndent())
                    .addParameter("username", username)
                    .executeAndFetchFirst(UserData::class.java)
        }
        return Optional.ofNullable(value)
    }

    private data class UserData(
            val id:Long,
            val password:String
    )

}