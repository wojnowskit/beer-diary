package beer.diary.web

import beer.diary.search.api.SearchExecutor
import beer.diary.web.ext.handleJson
import beer.diary.web.ext.toSearchCommand
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import io.javalin.http.Context

class BeerWebSearchConfiguration(
        private val app: Javalin,
        private val beerSearchExecutor: SearchExecutor
) {

    fun registerController() {
        val controller = BeerSearchController(beerSearchExecutor)
        app.routes {
            ApiBuilder.path("$API_BASE_PATH/search") {
                ApiBuilder.post("/", controller::searchBeers)
            }
        }
    }

}

private class BeerSearchController(
        private val beerSearchExecutor: SearchExecutor
) {

    fun searchBeers(ctx: Context) {
        val searchCommand = ctx.toSearchCommand()
        beerSearchExecutor
                .execute(searchCommand)
                .handleJson(ctx)
    }

}