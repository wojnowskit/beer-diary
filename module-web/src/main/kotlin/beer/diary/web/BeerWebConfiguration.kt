package beer.diary.web

import beer.diary.api.BeerApi
import beer.diary.api.beer.BeerDeleteCommand
import beer.diary.web.ext.*
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import io.javalin.http.Context

class BeerWebConfiguration(
        private val app: Javalin,
        private val beerApi: BeerApi
) {

    fun registerController() {
        val beerController = BeerController(beerApi)
        app.routes {
            ApiBuilder.path("$API_BASE_PATH/beer") {
                ApiBuilder.post("/create", beerController::createBeer)
                ApiBuilder.post("/update", beerController::updateBeer)
                ApiBuilder.get("/:beerId", beerController::readBeer)
                ApiBuilder.delete("/:beerId", beerController::deleteBeer)
            }
        }
    }

}

private class BeerController(private val beerApi: BeerApi) {

    fun createBeer(ctx: Context) {
        val createBeerCommand = ctx.toCreateBeerCommand()
        beerApi
                .createBeer(createBeerCommand)
                .handleJson(ctx)
    }

    fun updateBeer(ctx: Context) {
        val updateBeerCommand = ctx.toUpdateBeerCommand()
        beerApi
                .updateBeer(updateBeerCommand)
                .handleJson(ctx)
    }

    fun readBeer(ctx: Context) {
        val beerReadRequest = ctx.toBeerReadCommand()
        beerApi
                .readBeer(beerReadRequest)
                .handleJson(ctx)
    }

    fun deleteBeer(ctx: Context) {
        val deleteCommand: BeerDeleteCommand = ctx.toBeerDeleteCommand()
        beerApi
                .deleteBeer(deleteCommand)
                .handleJson(ctx)
    }

}