package beer.diary.web

import spock.lang.Unroll


class WebCORSTest extends WebLoggedAware {
    static def FIRST_DOMAIN = "http://test-one.com"
    static def SECOND_DOMAIN = "http://test-two.com:3000"
    static def TEST_ALLOWED_ORIGINS = [
            FIRST_DOMAIN,
            SECOND_DOMAIN
    ]

    @Unroll
    def "should provide header with allowed origin #domain"() {
        when:
            def headers = [
                    "Origin": domain
            ]
            def response = searchBeers(headers: headers)
        then:
            response.headers.getFirst("Access-Control-Allow-Origin") == domain
        where:
            domain | _
            FIRST_DOMAIN | _
            SECOND_DOMAIN | _
    }
    
    @Unroll
    def "should provide header with allowed method #method"() {
        when:
            def response = options("/")
                    .header("Access-Control-Request-Method", method)
                    .asObject(Map.class)
        then:
            response.headers.getFirst("Access-Control-Allow-Methods") == method
        where:
            method   | _
            "POST"   | _
            "GET"    | _
            "DELETE" | _
            "PUT"    | _
    }
    
}
