package beer.diary.web

class WebUpdateBeerControllerTest extends WebLoggedAware {

	def "should update added beer" () {
		given: "create beer"
			def beerId = createSampleBeerWeb().body.beerId
		and: "read added beer"
			def beer = getBeerWeb(beerId).body
		and: "copy beer data with name"
			def beerNoName = removeName(beer)
		and: "modify beer"
			beer["name"] = "updated name"
		when: "edit beer"
			def response = updateBeer(beer)
		and: "get edited beer"
			def editedBeer = getBeerWeb(beerId).body
		then:
			assertResponse(response)
		and: "name is edited"
			editedBeer["name"] == "updated name"
		and: "other fields not changed"
			beerNoName == removeName(editedBeer)
	}

	def removeName(map) {
		toJson(removeFromObject(map, "name"))
	}

}
