package beer.diary.web

class WebSearchBeerControllerTest extends WebLoggedAware {

	def "should search created beer by rest" () {
		given:
			createSampleBeerWeb()
		and:
			def searchParams = [
			    "searchBeersOwnerId": 1,
			    "searchText": "",
				"page": 1,
				"itemsPerPage": 10
			]
		when:
			def response = searchBeers(searchData: searchParams)
		then:
			assertResponse(response)
			response.body.items.size() == 1
	}

}
