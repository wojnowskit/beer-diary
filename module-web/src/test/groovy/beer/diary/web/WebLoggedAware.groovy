package beer.diary.web

import beer.diary.search.elastic.config.ElasticConfig
import beer.diary.search.elastic.config.ElasticSearchConfiguration

class WebLoggedAware extends WebSpecification {
    
    public static final String TEST_CREDENTIAL = "test"
    
    def searchConf = new ElasticSearchConfiguration(
            new ElasticConfig("localhost", 14532)
    )
    
    def searchIndexManager = searchConf.searchIndexManager()
    
    def setup() {
        addHashedUser(TEST_CREDENTIAL, TEST_CREDENTIAL)
        clearSearchIndex()
    }
    
    def clearSearchIndex() {
        searchIndexManager.clearIndexData()
        searchIndexManager.initIndexIfNotExists()
    }
    
    def sampleBeer(def params = [:]) {
        def beerData = [
                "name"          : "test beer",
                "description"   : "test-desc",
                "newNotes"      : [
                        [
                                "description": "note desc",
                                "createdDate": "2020-08-13 13:00"
                        ]
                ],
                "newIngredients": [
                        [
                                "name": "test name",
                                "info": "test info"
                        ]
                ]
        ]
        beerData + params
    }
    
    def createBeerWeb(def beer, String credentials = TEST_CREDENTIAL) {
        post("/api/beer/create")
                .basicAuth(credentials, credentials)
                .body(toJson(beer))
                .asObject(Map.class)
    }
    
    def createSampleBeerWeb(def params = [:], String credentials = TEST_CREDENTIAL) {
        def beer = sampleBeer(params)
        createBeerWeb(beer, credentials)
    }
    
    def getBeerWeb(def beerId, String credentials = TEST_CREDENTIAL) {
        get("/api/beer/${beerId}")
                .basicAuth(credentials, credentials)
                .asObject(Map.class)
    }
    
    def deleteBeerWeb(def beerId, String credentials = TEST_CREDENTIAL) {
        delete("/api/beer/${beerId}")
                .basicAuth(credentials, credentials)
                .asObject(Map.class)
    }
    
    def searchBeers(Map params) {
        def credentials = params.getOrDefault("credentials", TEST_CREDENTIAL)
        def body = toJson(params.getOrDefault("searchData", [:]))
        Map headers = params.getOrDefault("headers", [:])
        post("/api/search/")
                .basicAuth(credentials, credentials)
                .headers(headers)
                .body(body)
                .asObject(Map.class)
    }
    
    def updateBeer(def beer = [:], String credentials = TEST_CREDENTIAL) {
        post("/api/beer/update")
                .basicAuth(credentials, credentials)
                .body(toJson(beer))
                .asObject(Map.class)
    }
    
}
