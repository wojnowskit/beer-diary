package beer.diary.web

class WebReadBeerControllerTest extends WebLoggedAware {

	def "should read added beer" () {
		given:
			def beerId = createSampleBeerWeb().body.beerId
		when:
			def readBeerResponse = getBeerWeb(beerId)
		then:
			assertResponse(readBeerResponse)
			readBeerResponse.body
	}

	def "should not read beer added by another user" () {
		given: "two users"
			addHashedUser("user1")
			addHashedUser("user2")
		and: "created beer"
			def beerId = createSampleBeerWeb(credentials: "user1").body.beerId
		when:
			def readBeerResponse = getBeerWeb(beerId, "user2")
		then:
			readBeerResponse.status == 500
			readBeerResponse.body
	}

}
