package beer.diary.web

class WebBeerLoginControllerTest extends WebSpecification {

	def "should login with rest" () {
		given:
			addHashedUser("user1", "pass1")
			def body = """
					{
						"username": "user1",
						"password": "pass1"
					}
					"""
		when:
			def response = post("/login/")
				.body(body)
				.asObject(Map.class)
		then:
			assertResponse(response)
			response.body == ["hash":"Basic dXNlcjE6cGFzczE="]
	}

	def "should not login with rest" () {
		given:
			addHashedUser("user1", "pass1")
			def body = """
					{
						"username": "user1",
						"password": "DUMMY"
					}
					"""
		when:
			def response = post("/login/")
				.body(body)
				.asObject(Map.class)
		then:
			response.status == 500
			response.body.errors.size() == 1
	}

}
