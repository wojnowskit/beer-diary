package beer.diary.web

import beer.diary.base.BaseSpecification
import kong.unirest.ObjectMapper
import kong.unirest.ObjectResponse
import kong.unirest.Unirest

class WebSpecification extends BaseSpecification {

	void assertResponse(ObjectResponse response, status = 200) {
		assert response.headers["Content-Type"][0] == "application/json"
		assert response.status == status
	}
	
	def options(path) {
		Unirest.options("http://localhost:${TestSuiteWeb.port}${path}")
	}
	
	def post(path) {
		Unirest.post("http://localhost:${TestSuiteWeb.port}${path}")
			.withObjectMapper(unirestJsonMapper())
	}

	def get(path) {
		Unirest.get("http://localhost:${TestSuiteWeb.port}${path}")
			.withObjectMapper(unirestJsonMapper())
	}

	def delete(path) {
		Unirest.delete("http://localhost:${TestSuiteWeb.port}${path}")
			   .withObjectMapper(unirestJsonMapper())
	}

	private ObjectMapper unirestJsonMapper() {
		new ObjectMapper() {

			com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper()

			@Override
			<T> T readValue(final String value, final Class<T> valueType) {
				return mapper.readValue(value, valueType)
			}

			@Override
			String writeValue(final Object value) {
				return mapper.writeValueAsString(value)
			}
		}
	}

}
