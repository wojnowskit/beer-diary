package beer.diary.web

import beer.diary.datasource.BeerDiaryDbConfiguration
import beer.diary.datasource.UnitTestBeerDiaryDbConfiguration
import beer.diary.search.elastic.config.ElasticConfig
import beer.diary.web.security.WebSecurityTest
import io.javalin.Javalin
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite)
@Suite.SuiteClasses([
	WebBeerControllerTest,
	WebReadBeerControllerTest,
	WebDeleteBeerControllerTest,
	WebBeerLoginControllerTest,
	WebSearchBeerControllerTest,
	WebUpdateBeerControllerTest,
	WebSecurityTest,
	WebCORSTest
])
class TestSuiteWeb {

	static def port = 9097
	static Javalin app

	@BeforeClass
	static void beforeTests() throws Exception {
		OriginConfig originConfig = new OriginConfig(
				WebCORSTest.TEST_ALLOWED_ORIGINS
		)
		BeerDiaryDbConfiguration dbConf = new UnitTestBeerDiaryDbConfiguration()
		ElasticConfig elasticConfig = new ElasticConfig("localhost", 14532)
		app = MainConfigurationKt.registerApp(originConfig, dbConf, elasticConfig)
		app.start(port)
	}

	@AfterClass
	static void afterTests() throws Exception {
		app.stop()
		app = null
	}

}
