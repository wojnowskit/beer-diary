package beer.diary.web

class WebBeerControllerTest extends WebLoggedAware {

	def "should create beer by rest" () {
		given:
			def newBeer = sampleBeer()
		when:
			def response = createBeerWeb(newBeer)
		then:
			assertResponse(response)
			response.body["beerId"]
			response.body["noteIds"]
	}

	def "should not create beer by rest" () {
		given:
			def newBeer = sampleBeer(["name":""])
		when:
			def response = createBeerWeb(newBeer)
		then:
			assertResponse(response, 500)
			response.body["errors"]
	}

}
