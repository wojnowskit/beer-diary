package beer.diary.web.security

import beer.diary.base.BaseSpecification
import beer.diary.commons.fields.TextField
import beer.diary.commons.validation.ErrorCode
import spock.lang.Subject
import spock.lang.Unroll

class LoginExecutorTest extends BaseSpecification {

	@Subject
	LoginExecutor loginExecutor

	def setup() {
		loginExecutor = new SimpleLoginExecutor(
			new SimpleSecurityUserRepository(dbConf.sql2o())
		)
	}

	def "should handle login"() {
		given:
			addHashedUser("user1", "pass1")
			def command = new LoginCommand(new TextField("username", "user1"),
										   new TextField("password", "pass1"))
		when:
			def result = loginExecutor.execute(command)
		then:
			result.isRight()
			assertLoginResult(result.get())
	}

	void assertLoginResult(LoginResult loginResult) {
		def hashValue = loginResult.hash.replace("Basic ", "")
		def decoded = new String(Base64.getDecoder().decode(hashValue))
		assert decoded == "user1:pass1"
	}

	@Unroll
	def "should handle login with wrong data"() {
		given:
			addHashedUser("user1", "pass1")
			def command = new LoginCommand(new TextField("username", username),
										   new TextField("password", password))
		when:
			def result = loginExecutor.execute(command)
		then:
			result.isLeft()
			result.left.errors == expectedErrors
		where:
			username | password || expectedErrors
			""       | ""       || expectedErrors1()
			"user1"  | ""       || expectedErrors2()
			"user1"  | "SSAP"   || expectedErrors3()
	}

	def expectedErrors1() {
		[
			new ErrorCode("username","text.field.error.username"),
			new ErrorCode("password","text.field.error.password")
		]
	}

	def expectedErrors2() {
		[
			new ErrorCode("password","text.field.error.password"),
		]
	}

	def expectedErrors3() {
		[
			new ErrorCode("credentials","credentials.data.error"),
		]
	}

}
