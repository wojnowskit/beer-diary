package beer.diary.web.security

import beer.diary.web.WebSpecification

class WebSecurityTest extends WebSpecification {
    
    def "should require basic auth header" () {
        when:
            def response = post("/api/beer/create")
                    .body("{}")
                    .asObject(Map.class)
        then:
            response.status == 401
    }
    
    def "should validate basic auth header" () {
        given:
            addHashedUser("tomek", "1234")
        when:
            def response = post("/api/beer/create")
                    .basicAuth("tomek","dummy-not-match")
                    .body("{}")
                    .asObject(Map.class)
        then:
            response.status == 401
    }
    
}
