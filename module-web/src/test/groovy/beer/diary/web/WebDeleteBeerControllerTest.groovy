package beer.diary.web

class WebDeleteBeerControllerTest extends WebLoggedAware {

	def "should delete added beer" () {
		given:
			def beerId = createSampleBeerWeb().body.beerId
		when:
			def response = deleteBeerWeb(beerId)
		then:
			assertResponse(response)
			response.body
	}

	def "should not delete beer added by another user" () {
		given: "two users"
			addHashedUser("user1")
			addHashedUser("user2")
		and: "created beer"
			def beerId = createSampleBeerWeb(credentials: "user1").body.beerId
		when:
			def response = deleteBeerWeb(beerId, "user2")
		then:
			response.status == 500
			response.body
	}

}
