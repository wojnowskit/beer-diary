package beer.diary.commons.fields

import spock.lang.Specification
import spock.lang.Unroll

class DiaryDateTest extends Specification {

	@Unroll
	def "should validate diary date"() {
		given:
			def diaryDate = DiaryDate.@Companion.of(dateValue)
		when:
			def res = diaryDate.validate()
		then:
			res.isValid() == expectedValid
		where:
			dateValue          || expectedValid
			""                 || false
			"dsad"             || false
			"dsad  sd"         || false
			"235467"           || false
			"2020-01-16 12:45" || true
	}

}
