package beer.diary.commons.fields

import beer.diary.commons.validation.ValidData
import beer.diary.commons.validation.ValidationResult
import beer.diary.commons.validation.ValidationResultFactory
import io.vavr.control.Try
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class PositiveNumber (
        val value: Long
) : ValidData {
    override fun validate(): ValidationResult {
        if (value < 1)
            return ValidationResultFactory.failureOf("number","error")
        return ValidationResultFactory.success()
    }

    fun intValue(): Int = value.toInt()

}


data class TextField(
        val key:String,
        private val value:String
) : ValidData {

    private var _value = value.trim()

    override fun validate(): ValidationResult {
        if (_value.isBlank())
            return ValidationResultFactory.failureOf(key, "text.field.error.${key}")
        return ValidationResultFactory.success()
    }

    fun value() = _value

}

const val DATE_FORMAT = "yyyy-MM-dd HH:mm"
val FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT)

data class DiaryDate private constructor(
    private val value: String
) : ValidData {

    companion object {
        fun of (value: String): DiaryDate {
            return DiaryDate(value)
        }
        fun ofNow (): DiaryDate {
            return DiaryDate(
                    FORMATTER.format(
                            LocalDateTime.now()
                    )
            )
        }
    }

    override fun validate(): ValidationResult {
        if (value.isBlank())
            return ValidationResultFactory.failureOf("date", "wrong date")
        val parseTry = Try.of { LocalDateTime.parse(value, FORMATTER) }
        if (parseTry.isFailure)
            return ValidationResultFactory.failureOf("date", "wrong date")
        return ValidationResultFactory.success()
    }

    fun date() = LocalDateTime.parse(value, FORMATTER)
}