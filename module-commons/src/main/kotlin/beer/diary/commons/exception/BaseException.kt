package beer.diary.commons.exception

import beer.diary.commons.messages.MessagesResolver
import beer.diary.commons.validation.ErrorCode

open class BaseException(val errors: List<ErrorCode>) : Exception("") {

    private val messageResolver = MessagesResolver

    fun errorCode() = 500

    fun info() = mapOf(
        "errors" to messageResolver.parseErrors(errors)
    )
}