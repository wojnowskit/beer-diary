package beer.diary.commons.validation


data class ErrorCode(
        val field: String,
        val code: String
) : Comparable<ErrorCode> {
    override fun compareTo(other: ErrorCode): Int {
        return field.compareTo(other.field)
    }
}

interface ValidationConstraints {
    fun isValid(): Boolean
    fun isNotValid(): Boolean
    fun errors(): List<ErrorCode>
}

sealed class ValidationResult: ValidationConstraints {

    data class Failure(private val errors: List<ErrorCode>): ValidationResult() {
        override fun isValid() = false

        override fun isNotValid() = !isValid()

        override fun errors(): List<ErrorCode> {
            return errors
        }
    }

    object Success : ValidationResult() {
        override fun isValid() = true

        override fun isNotValid() = !isValid()

        override fun errors() = listOf<ErrorCode>()
    }

}

interface ValidData {
    fun validate(): ValidationResult
}

abstract class MultipleValidData : ValidData {
    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                dataToValidate()
        )
    }

    abstract fun dataToValidate(): List<ValidData>
}

object ValidationResultFactory {

    fun success() = ValidationResult.Success

    fun failureOf(errorCode: ErrorCode) = ValidationResult.Failure(listOf(errorCode))

    fun failureOf(field: String, errorCode: String) =
            ValidationResult.Failure(listOf(ErrorCode(field, errorCode)))

    fun failureOf(errors: List<ErrorCode>) = ValidationResult.Failure(errors)

    fun ofMultiple(validData: List<ValidData>): ValidationResult {
        val errorCodes: List<ErrorCode> = validData.map(ValidData::validate)
                .filter(ValidationResult::isNotValid)
                .map(ValidationResult::errors)
                .flatten()
        return if (errorCodes.isNotEmpty()) {
            failureOf(errorCodes)
        } else success()
    }

}