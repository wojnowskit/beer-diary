package beer.diary.commons.executors

import beer.diary.commons.exception.BaseException
import beer.diary.commons.validation.ErrorCode
import beer.diary.commons.validation.ValidData
import io.vavr.control.Either

interface Executor<T, R, E> where T : ValidData, E :BaseException {

    fun execute(executeCommand:T): Either<E, R>

}

abstract class BaseExecutor<T, R> : Executor<T, R, BaseException> where T : ValidData {

    abstract fun executeValidated(executeCommand:T) : R

    fun exception(errors: List<ErrorCode>): BaseException = BaseException(errors)

    override fun execute(executeCommand: T): Either<BaseException, R> {
        val validate = executeCommand.validate()
        if (validate.isNotValid()) {
            return Either.left(exception(validate.errors()))
        }
        return Either.right(executeValidated(executeCommand))
    }
}

abstract class MultiValidateExecutor<T, R, E>: Executor<T, R, E> where T : ValidData, E :BaseException {

    override fun execute(executeCommand: T): Either<E, R> {
        val validate = executeCommand.validate()
        if (validate.isNotValid()) {
            return Either.left(BaseException(validate.errors()) as E)
        }
        return executeValidated(executeCommand)
    }

    abstract fun executeValidated(validatedCommand: T): Either<E, R>

}