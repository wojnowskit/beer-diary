package beer.diary.commons.messages

import beer.diary.commons.validation.ErrorCode
import java.io.InputStreamReader
import java.nio.charset.Charset
import java.util.*


object MessagesResolver {

    private var messages: Properties

    init {
        messages = parseProperties()
    }

    private fun parseProperties(): Properties {
        val bytes = MessagesResolver::class.java.getResource("/messages.properties").openStream()
        val properties = Properties()
        properties.load(InputStreamReader(bytes, Charset.forName("UTF-8")))
        return properties
    }

    fun getMessage(key: String) : String {
        return messages.getProperty(key) ?: "???"
    }

    fun parseErrors(errors : List<ErrorCode>) : Map<String,String> {
        return errors.map {
            it.field to getMessage(it.code)
        }.toMap()
    }

}