package beer.diary.commons.resource

import java.net.URL

fun readAsString(url: URL) = url.readText(Charsets.UTF_8)