package beer.diary.search.elastic.api.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
public class BeerCreatedEvent {
	private final Long beerId;
	private final Long ownerId;
	private final String name;
	private final LocalDateTime createdAt;

	public String beerIdAsString() {
		return String.valueOf(beerId);
	}
}
