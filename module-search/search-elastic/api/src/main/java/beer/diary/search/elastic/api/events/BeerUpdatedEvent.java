package beer.diary.search.elastic.api.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BeerUpdatedEvent {
	private final Long beerId;
	private final String name;

	public String beerIdAsString() {
		return String.valueOf(beerId);
	}
}
