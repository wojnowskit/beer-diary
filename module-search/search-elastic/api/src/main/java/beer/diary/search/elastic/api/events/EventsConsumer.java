package beer.diary.search.elastic.api.events;

import io.vavr.control.Either;

public interface EventsConsumer {

	Either<ConsumingException, EventConsumed> consumeCreateEvent(BeerCreatedEvent beerCreatedEvent);
	Either<ConsumingException, EventConsumed> consumeUpdateEvent(BeerUpdatedEvent beerUpdatedEvent);
	Either<ConsumingException, EventConsumed> consumeDeleteEvent(BeerDeletedEvent beerDeletedEvent);

	class ConsumingException extends RuntimeException {

		public ConsumingException() {
			super("Consuming exception");
		}
	}

	class EventConsumed {

	}

}
