package beer.diary.search.elastic.api.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BeerDeletedEvent {
	private final Long beerId;
}
