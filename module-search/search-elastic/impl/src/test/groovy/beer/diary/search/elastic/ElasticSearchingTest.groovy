package beer.diary.search.elastic


class ElasticSearchingTest extends ElasticSpecification {

	def "should search for beer name ignore case" () {
		given:
			addBeer([
				"beerId": 1,
			    "name": "TesT BEER"
			])
		when:
			def result = search([
			    "searchText": "test beer"
			])
		then:
			result.items == [
			    item(1, "TesT BEER")
			]
	}

	def "should search for beer name misspelled" () {
		given:
			addBeer([
				"beerId": 1,
				"name": "TesT BEER"
			])
		when:
			def result = search([
				"searchText": "tet bear"
			])
		then:
			result.items == [
				item(1, "TesT BEER")
			]
	}

	def "should not search for beer name not exists" () {
		given:
			addBeer([
				"beerId": 1,
				"name": "TesT BEER"
			])
		when:
			def result = search([
				"searchText": "pszenica"
			])
		then:
			result.items == []
	}

	def "should search for multiple beers name misspelled" () {
		given:
			addBeer([
				"beerId": 1,
				"name": "ipa beer"
			])
			addBeer([
				"beerId": 2,
				"name": "ipa second beer"
			])
			addBeer([
				"beerId": 3,
				"name": "another ipa beer"
			])
			addBeer([
				"beerId": 4,
				"name": "white beer"
			])
		when:
			def result = search([
				"searchText": "aipa"
			])
		then:
			result.items == [
				item(1, "ipa beer"),
				item(2, "ipa second beer"),
				item(3, "another ipa beer"),
			]
	}

}
