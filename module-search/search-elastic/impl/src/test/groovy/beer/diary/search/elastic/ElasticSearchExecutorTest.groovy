package beer.diary.search.elastic

import beer.diary.search.api.PaginationResult
import beer.diary.search.api.SearchResultItem

class ElasticSearchExecutorTest extends ElasticSpecification {

	def "should search for all beers" () {
		given:
			addBeer([
			    "beerId": 1L
			])
		when:
			def result = search()
		then:
			noExceptionThrown()
			result.items == [
			    new SearchResultItem(1L, "test beer")
			]
			result.pagination == new PaginationResult(1, 1, false)
	}

	def "should search for all beers for user paginate" () {
		given:
			(1..2).each {
				addBeer([
					"beerId": it,
					"ownerId": 1L,
					"name": "beer from user1"
				])
			}
			addBeer([
				"beerId": 30L,
				"ownerId": 2L,
				"name": "beer from user2"
			])
		when:
			def result = search()
		then:
			noExceptionThrown()
			result.items == [
				new SearchResultItem(1L, "beer from user1"),
				new SearchResultItem(2L, "beer from user1")
			]
			result.pagination == new PaginationResult(2, 1, false)
	}

	def "should search with paginate" () {
		given:
			(1..15).each {
				addBeer([
					"beerId": it,
					"ownerId": 1L,
					"name": "beer from user1",
				])
			}
		when:
			def result = search([
			    "page": 1,
				"itemsPerPage": 5
			])
		then:
			result.items == [
				new SearchResultItem(1L, "beer from user1"),
				new SearchResultItem(2L, "beer from user1"),
				new SearchResultItem(3L, "beer from user1"),
				new SearchResultItem(4L, "beer from user1"),
				new SearchResultItem(5L, "beer from user1"),
			]
			result.pagination == new PaginationResult(15, 1, true)
		when:
			result = search([
				"page": 2,
				"itemsPerPage": 5
			])
		then:
			result.items == [
				new SearchResultItem(6L, "beer from user1"),
				new SearchResultItem(7L, "beer from user1"),
				new SearchResultItem(8L, "beer from user1"),
				new SearchResultItem(9L, "beer from user1"),
				new SearchResultItem(10L, "beer from user1"),
			]
			result.pagination == new PaginationResult(15, 2, true)
		when:
			result = search([
				"page": 3,
				"itemsPerPage": 5
			])
		then:
			result.items == [
				new SearchResultItem(11L, "beer from user1"),
				new SearchResultItem(12L, "beer from user1"),
				new SearchResultItem(13L, "beer from user1"),
				new SearchResultItem(14L, "beer from user1"),
				new SearchResultItem(15L, "beer from user1"),
			]
			result.pagination == new PaginationResult(15, 3, false)
	}

}
