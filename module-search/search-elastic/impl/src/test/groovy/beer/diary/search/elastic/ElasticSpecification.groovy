package beer.diary.search.elastic

import beer.diary.commons.fields.PositiveNumber
import beer.diary.search.api.PaginateCommand
import beer.diary.search.api.SearchCommand
import beer.diary.search.api.SearchData
import beer.diary.search.api.SearchExecutor
import beer.diary.search.api.SearchResultItem
import beer.diary.search.api.fields.SearchTextField
import beer.diary.search.elastic.api.events.BeerCreatedEvent
import beer.diary.search.elastic.api.events.EventsConsumer
import beer.diary.search.elastic.config.ElasticConfig
import beer.diary.search.elastic.config.ElasticSearchConfiguration
import spock.lang.Specification

import java.time.LocalDateTime

class ElasticSpecification extends Specification {

	private def config = new ElasticSearchConfiguration(
		new ElasticConfig(
			"localhost",
			14532
		)
	)

	private SearchExecutor searchExecutor = config.searchExecutor()
	private EventsConsumer eventsConsumer = config.eventsConsumer()
	private SearchIndexManager searchIndexManager = config.searchIndexManager()

	def setup() {
		searchIndexManager.clearIndexData()
		searchIndexManager.initIndexIfNotExists()
	}

	def search(Map params = [:]) {
		searchExecutor.execute(
			new SearchCommand(
				new SearchTextField(params.getOrDefault("searchText", "")),
				new SearchData(
					new PositiveNumber(
						params.getOrDefault("searchUserId", 1L)
					)
				),
				new PaginateCommand(
					new PositiveNumber(
						params.getOrDefault("page", 1L)
					),
					new PositiveNumber(
							params.getOrDefault("itemsPerPage", 10)
					)
				)
			)
		).get()
	}

	def addBeer(Map params = [:]) {
		eventsConsumer.consumeCreateEvent(
			new BeerCreatedEvent(
				params["beerId"],
				params.getOrDefault("ownerId", 1L),
				params.getOrDefault("name", "test beer"),
				params.getOrDefault("createdAt", nowPlusMinutes(params["beerId"]))
			)
		)
	}

	def nowPlusMinutes(minutes) {
		LocalDateTime.now().plusMinutes(minutes)
	}
	
	def item(id, name) {
		new SearchResultItem(id, name)
	}

}
