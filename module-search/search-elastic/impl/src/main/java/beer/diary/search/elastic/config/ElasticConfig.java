package beer.diary.search.elastic.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class ElasticConfig {
	private final String host;
	private final Integer port;
}
