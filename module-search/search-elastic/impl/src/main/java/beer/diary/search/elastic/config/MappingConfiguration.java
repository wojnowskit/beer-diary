package beer.diary.search.elastic.config;

import beer.diary.commons.resource.ResourceReaderKt;
import beer.diary.search.elastic.SearchIndexManager;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import static beer.diary.commons.fields.FieldsKt.DATE_FORMAT;

public class MappingConfiguration {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final JsonSerializer<LocalDateTime> DATE_TIME_JSON_SERIALIZER = new JsonSerializer<>() {
        @Override
        public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(value.format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
        }
    };

    private final String mapping;

    public MappingConfiguration() {
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(LocalDateTime.class, DATE_TIME_JSON_SERIALIZER);
        OBJECT_MAPPER.registerModule(simpleModule);
        mapping = readJsonMapping();
    }

    private String readJsonMapping() {
        URL url = SearchIndexManager.class.getResource("/beer_index.json");
        return ResourceReaderKt.readAsString(url)
                .replaceAll("#dateFormat", DATE_FORMAT);
    }

    public String mapping() {
        return mapping;
    }

    public HashMap convertValue(Object data) {
        return OBJECT_MAPPER.convertValue(data, HashMap.class);
    }

}
