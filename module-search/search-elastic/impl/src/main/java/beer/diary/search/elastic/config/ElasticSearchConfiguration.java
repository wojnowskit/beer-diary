package beer.diary.search.elastic.config;

import beer.diary.search.api.SearchExecutor;
import beer.diary.search.elastic.*;
import beer.diary.search.elastic.api.events.EventsConsumer;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

public class ElasticSearchConfiguration {

	private final MappingConfiguration mappingConfiguration = new MappingConfiguration();
	private final RestHighLevelClient elasticClient;

	public ElasticSearchConfiguration(ElasticConfig elasticConfig) {
		elasticClient = new RestHighLevelClient(
			RestClient.builder(new HttpHost(elasticConfig.getHost(), elasticConfig.getPort(), "http"))
		);
	}

	public SearchExecutor searchExecutor() {
		return new ElasticSearchExecutor(elasticClient);
	}

	public EventsConsumer eventsConsumer() {
		return new SimpleEventsConsumer(elasticClient, mappingConfiguration);
	}

	public SearchIndexManager searchIndexManager() {
		return new SearchIndexManager(elasticClient, mappingConfiguration);
	}

}
