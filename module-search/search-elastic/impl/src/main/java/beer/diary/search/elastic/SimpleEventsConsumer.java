package beer.diary.search.elastic;

import beer.diary.search.elastic.api.events.BeerCreatedEvent;
import beer.diary.search.elastic.api.events.BeerDeletedEvent;
import beer.diary.search.elastic.api.events.BeerUpdatedEvent;
import beer.diary.search.elastic.api.events.EventsConsumer;
import beer.diary.search.elastic.config.MappingConfiguration;
import io.vavr.CheckedFunction0;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;
import java.util.HashMap;

import static beer.diary.search.elastic.SearchIndexManager.SEARCH_INDEX;

@RequiredArgsConstructor
public class SimpleEventsConsumer implements EventsConsumer {

	private final RestHighLevelClient client;
	private final MappingConfiguration mappingConfiguration;

	@Override
	public Either<ConsumingException, EventConsumed> consumeCreateEvent(final BeerCreatedEvent beerCreatedEvent) {
		return executeConsume(() -> consumeCreate(beerCreatedEvent));
	}

	private EventConsumed consumeCreate(final BeerCreatedEvent beerCreatedEvent) throws IOException {
		HashMap data = mappingConfiguration.convertValue(beerCreatedEvent);
		IndexRequest indexRequest = new IndexRequest(SEARCH_INDEX)
			.source(data)
			.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE)
			.id(beerCreatedEvent.beerIdAsString());
		client.index(indexRequest, RequestOptions.DEFAULT);
		return new EventConsumed();
	}

	@Override
	public Either<ConsumingException, EventConsumed> consumeUpdateEvent(final BeerUpdatedEvent beerUpdatedEvent) {
		return executeConsume(() -> consumeUpdate(beerUpdatedEvent));
	}

	private EventConsumed consumeUpdate(final BeerUpdatedEvent beerUpdatedEvent) throws IOException {
		HashMap data = mappingConfiguration.convertValue(beerUpdatedEvent);
		UpdateRequest updateRequest = new UpdateRequest(SEARCH_INDEX, beerUpdatedEvent.beerIdAsString())
			.doc(data)
			.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
		client.update(updateRequest, RequestOptions.DEFAULT);
		return new EventConsumed();
	}

	@Override
	public Either<ConsumingException, EventConsumed> consumeDeleteEvent(final BeerDeletedEvent beerDeletedEvent) {
		return executeConsume(() -> consumeDelete(beerDeletedEvent));
	}

	private EventConsumed consumeDelete(final BeerDeletedEvent beerDeletedEvent) throws IOException {
		DeleteRequest request = new DeleteRequest(SEARCH_INDEX, beerDeletedEvent.getBeerId().toString())
			.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
		client.delete(request, RequestOptions.DEFAULT);
		return new EventConsumed();
	}

	private Either<ConsumingException, EventConsumed> executeConsume(CheckedFunction0<EventConsumed> supplier) {
		return Try.of(supplier).toEither().mapLeft(ex -> new ConsumingException());
	}

}
