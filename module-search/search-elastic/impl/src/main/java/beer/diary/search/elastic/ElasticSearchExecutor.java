package beer.diary.search.elastic;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import beer.diary.search.api.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;

import static beer.diary.search.elastic.SearchIndexManager.SEARCH_INDEX;

@RequiredArgsConstructor
public class ElasticSearchExecutor extends SearchExecutor {

	private final RestHighLevelClient client;

	@Override
	public Either<SearchException, SearchResult> executeValidated(final SearchCommand command) {
		return Either.right(getSearchResult(command.getSearchTextField().getSearchText(),
											command.getSearchData().getSearchUserId().getValue(),
											command.getPaginate()));
	}

	private SearchResult getSearchResult(final String searchText, Long searchBeersOwnerId, PaginateCommand paginate) {
		var request = prepareRequest(searchText, searchBeersOwnerId, paginate);
		var hits = searchHits(request);
		return new SearchResult(mapItems(hits), buildPagination(paginate, hits));
	}

	private SearchHits searchHits(SearchRequest request) {
		return Try.of(() -> {
			SearchResponse response = client.search(request, RequestOptions.DEFAULT);
			return response.getHits();
		}).getOrElseThrow(throwable -> new RuntimeException(throwable));
	}

	private PaginationResult buildPagination(final PaginateCommand paginate, final SearchHits hits) {
		var totalItems = hits.getTotalHits().value;
		var pageNumber = paginate.getPage().intValue();
		var hasNext = pageNumber * paginate.getItemsPerPage().intValue() < totalItems;
		return new PaginationResult(totalItems, pageNumber, hasNext);
	}

	private List<SearchResultItem> mapItems(final SearchHits hits) {
		return Arrays.stream(hits.getHits()).map(this::mapResult).collect(Collectors.toList());
	}

	private SearchRequest prepareRequest(final String searchText, final Long searchBeersOwnerId,
										 final PaginateCommand paginate) {
		MatchQueryBuilder matchOwnerIdQuery = QueryBuilders.matchQuery("ownerId", searchBeersOwnerId);
		var query = QueryBuilders.boolQuery().must(matchOwnerIdQuery);
		handleSearchText(searchText, query);
		int itemsPerPage = paginate.getItemsPerPage().intValue();
		var from = buildFrom(paginate.getPage().intValue(), itemsPerPage);
		var sort = SortBuilders.fieldSort("createdAt").order(SortOrder.ASC);
		var sourceBuilder = new SearchSourceBuilder().query(query).sort(sort).from(from).size(itemsPerPage);
		return searchRequest().source(sourceBuilder);
	}

	private void handleSearchText(final String searchText, final BoolQueryBuilder query) {
		Arrays.stream(searchText.split(" "))
			  .filter(token -> !token.isBlank())
			  .forEach(token -> query.must(QueryBuilders.fuzzyQuery("name", token)));
	}

	private int buildFrom(final Integer page, final Integer itemsPerPage) {
		return ((page - 1) * itemsPerPage);
	}

	private SearchResultItem mapResult(final SearchHit searchHit) {
		var source = searchHit.getSourceAsMap();
		return new SearchResultItem(Long.valueOf(source.get("beerId").toString()), source.get("name").toString());
	}

	private SearchRequest searchRequest() {
		return new SearchRequest(SEARCH_INDEX);
	}

}
