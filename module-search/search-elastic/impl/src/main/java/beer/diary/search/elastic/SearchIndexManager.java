package beer.diary.search.elastic;

import beer.diary.search.elastic.config.MappingConfiguration;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
@Slf4j
public class SearchIndexManager {

    public static String SEARCH_INDEX = "beer_index";

    private final RestHighLevelClient client;
    private final MappingConfiguration mappingConfiguration;

    public void initIndexIfNotExists() {
        initIndexRetry();
    }

    private void handleInit() {
        if (indexExists())
            return;
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(SEARCH_INDEX);
        createIndexRequest.mapping(mappingConfiguration.mapping(), XContentType.JSON);
        Try.of(() -> client.indices().create(createIndexRequest, RequestOptions.DEFAULT)).get();
    }

    private void initIndexRetry() {
        AtomicInteger integer = new AtomicInteger(0);
        AtomicBoolean running = new AtomicBoolean(true);
        while (running.get()) {
            int count = integer.incrementAndGet();
            log.warn("COUNT " + count);
            if (count > 20) {
                throw new RuntimeException("Can't connect to elasticsearch");
            }
            log.warn("ELASTIC INDEX TRY");
            try {
                handleInit();
                running.set(false);
                log.warn("ELASTIC INDEX DONE");
            } catch (Exception e) {
                log.warn("ELASTIC INDEX RETRY ON ERROR " + e.getMessage());
                running.set(true);
            }
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clearIndexData() {
        if (indexExists()) {
            Try.of(() -> {
                DeleteIndexRequest request = new DeleteIndexRequest(SEARCH_INDEX);
                return client.indices().delete(request, RequestOptions.DEFAULT);
            }).get();
        }
    }

    private boolean indexExists() {
        return Try.of(() -> {
            GetIndexRequest indexRequest = new GetIndexRequest(SEARCH_INDEX);
            return client.indices().exists(indexRequest, RequestOptions.DEFAULT);
        })
                .get();
    }

}
