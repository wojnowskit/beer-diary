package beer.diary.search.api

import beer.diary.commons.fields.PositiveNumber
import beer.diary.search.api.fields.SearchTextField
import spock.lang.Specification
import spock.lang.Unroll

import static beer.diary.commons.validation.ValidationResultFactory.INSTANCE as VRF

class SearchCommandTest extends Specification {

	@Unroll
	def "should validate search command searchText"() {
		given:
			def command = new SearchCommand(
				new SearchTextField(searchText),
				searchData(),
				paginate()
			)
		when:
			def validate = command.validate()
		then:
			validate == expectedValidate
			command.searchTextField.searchText == expectedSearchPhrase
		where:
			searchText | expectedSearchPhrase || expectedValidate
			null       | ""                   || VRF.failureOf("searchText", "error.code.searchText")
			""         | ""                   || VRF.success()
			" a b c "  | "a b c"              || VRF.success()
			"a b c"    | "a b c"              || VRF.success()
			"abc"      | "abc"                || VRF.success()
	}

	def searchData() {
		new SearchData(
			new PositiveNumber(1L)
		)
	}

	def paginate() {
		new PaginateCommand(
			new PositiveNumber(1),
			new PositiveNumber(10)
		)
	}

}
