package beer.diary.search.api.fields;

import beer.diary.commons.validation.*;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SearchTextField implements ValidData {
	private final String searchText;

	@Override
	public ValidationResult validate() {
		if (searchText == null) {
			return ValidationResultFactory.INSTANCE.failureOf("searchText", "error.code.searchText");
		}
		return ValidationResultFactory.INSTANCE.success();
	}

	public String getSearchText() {
		return searchText == null ? "" : searchText.trim();
	}
}
