package beer.diary.search.api;

import beer.diary.commons.executors.MultiValidateExecutor;

public abstract class SearchExecutor extends MultiValidateExecutor<SearchCommand, SearchResult, SearchException> {

}
