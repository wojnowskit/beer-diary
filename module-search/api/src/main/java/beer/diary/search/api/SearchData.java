package beer.diary.search.api;

import java.util.List;

import beer.diary.commons.fields.PositiveNumber;
import beer.diary.commons.validation.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
@Getter
public class SearchData extends MultipleValidData {

	private final PositiveNumber searchUserId;

	@NotNull
	@Override
	public List<ValidData> dataToValidate() {
		return List.of(searchUserId);
	}

}
