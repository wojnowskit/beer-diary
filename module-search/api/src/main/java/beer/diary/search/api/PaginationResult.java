package beer.diary.search.api;

import lombok.*;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
public class PaginationResult {
	private final Long total;
	private final Integer currentPage;
	private final boolean hasNextPage;
}
