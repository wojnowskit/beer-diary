package beer.diary.search.api;

import lombok.*;

@RequiredArgsConstructor
@Builder
@Getter
@ToString
@EqualsAndHashCode
public class SearchResultItem {
	private final Long id;
	private final String name;
}
