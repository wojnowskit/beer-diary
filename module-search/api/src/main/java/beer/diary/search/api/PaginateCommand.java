package beer.diary.search.api;

import java.util.List;

import beer.diary.commons.fields.PositiveNumber;
import beer.diary.commons.validation.MultipleValidData;
import beer.diary.commons.validation.ValidData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
@Getter
public class PaginateCommand extends MultipleValidData {

	private final PositiveNumber page;
	private final PositiveNumber itemsPerPage;

	@NotNull
	@Override
	public List<ValidData> dataToValidate() {
		return List.of(page, itemsPerPage);
	}

}
