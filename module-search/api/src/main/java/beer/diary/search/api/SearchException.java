package beer.diary.search.api;

import java.util.List;

import beer.diary.commons.exception.BaseException;
import beer.diary.commons.validation.ErrorCode;
import org.jetbrains.annotations.NotNull;

public class SearchException extends BaseException {

	public SearchException(@NotNull final List<ErrorCode> errors) {
		super(errors);
	}
}
