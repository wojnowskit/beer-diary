package beer.diary.search.api;

import java.util.List;

import beer.diary.commons.validation.*;
import beer.diary.search.api.fields.SearchTextField;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
@Getter
public class SearchCommand extends MultipleValidData {

	private final SearchTextField searchTextField;
	private final SearchData searchData;
	private final PaginateCommand paginate;

	@NotNull
	@Override
	public List<ValidData> dataToValidate() {
		return List.of(searchData, searchTextField, paginate);
	}

}
