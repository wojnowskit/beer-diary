package beer.diary.search.api;

import java.util.Collections;
import java.util.List;

import lombok.*;

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class SearchResult {
	private final List<SearchResultItem> items;
	private final PaginationResult pagination;

	public List<SearchResultItem> getItems() {
		return Collections.unmodifiableList(items);
	}

}
