package beer.diary

import beer.diary.datasource.BeerDiaryDbParams
import beer.diary.datasource.DefaultBeerDiaryDbConfiguration
import beer.diary.search.elastic.config.ElasticConfig
import beer.diary.web.OriginConfig
import beer.diary.web.registerApp
import org.slf4j.LoggerFactory

var log = LoggerFactory.getLogger("MAIN")

fun main(args: Array<String>) {
    val originConfig = OriginConfig(listOf(
            System.getenv("APP_ALLOWED_ORIGIN")
    ))
    val beerDiaryDbParams = BeerDiaryDbParams(
            System.getenv("APP_DB_HOST"),
            System.getenv("APP_DB_NAME"),
            System.getenv("APP_DB_USER"),
            System.getenv("APP_DB_PASS"),
            System.getenv("APP_DB_PORT").toInt()
    )
    log.warn(beerDiaryDbParams.toString())
    val dbConfiguration = DefaultBeerDiaryDbConfiguration(
            beerDiaryDbParams
    )
    val elasticConfig = ElasticConfig(
            System.getenv("APP_ELASTIC_HOST"),
            System.getenv("APP_ELASTIC_PORT").toInt()
    )
    log.warn(elasticConfig.toString())
    val app = registerApp(originConfig, dbConfiguration, elasticConfig)
    app.start(8082)
}