package beer.diary.api.beer

import beer.diary.api.beer.ingredient.*
import beer.diary.api.beer.note.BeerNote
import beer.diary.api.beer.note.CreateBeerNotes
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.api.beer.note.Note
import beer.diary.commons.exception.BaseException
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import beer.diary.commons.validation.ErrorCode
import beer.diary.commons.validation.ValidData
import beer.diary.commons.validation.ValidationResult
import beer.diary.commons.validation.ValidationResultFactory

data class BeerOwnerId (
        val positiveNumber: PositiveNumber
) : ValidData {
    override fun validate(): ValidationResult {
        return positiveNumber.validate()
    }

    fun value() = positiveNumber.value
    fun match(beerOwnerId: Long) = this == BeerOwnerId(PositiveNumber(beerOwnerId))
    fun notMatch(beerOwnerId: Long) = !match(beerOwnerId)

}

data class CreateBeerCommand(
        val name: TextField,
        val description: TextField,
        val beerOwnerId: BeerOwnerId,
        val createdDate: DiaryDate,
        val newNotes: List<NewBeerNote>,
        val newIngredients: List<NewBeerIngredient>
) : ValidData {

    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                listOf(name, description, beerOwnerId, createdDate) + newNotes + newIngredients
        )
    }

    fun toCreateBeerNoteCommand(beerId: Long) = CreateBeerNotes(
            beerId,
            newNotes
    )

    fun toCreateBerIngredientsCommand(beerId: Long) = CreateBeerIngredientsCommand(
            beerId,
            newIngredients
    )

}

data class BeerCreatedResponse(
        val beerId: Long,
        val noteIds: List<Long>,
        val ingredientIds: List<Long>
)

data class BeerDeleteCommand(
        val beerId: PositiveNumber,
        val beerOwnerId: BeerOwnerId
) : ValidData {
    override fun validate(): ValidationResult {
        return beerId.validate()
    }
}

data class BeerDeleteResult(
        val beerId: Long
)

class BeerDeleteException(errors: List<ErrorCode>) : BaseException(errors)

data class BeerReadCommand(
        val beerId: PositiveNumber,
        val beerOwnerId: BeerOwnerId
) : ValidData {
    override fun validate(): ValidationResult {
        return beerId.validate()
    }
}

data class BeerReadResult(
        val beerId: Long,
        val name: String,
        val description: String,
        val ingredients: Set<Ingredient>,
        val notes: Set<Note>
)

class BeerReadException(errors: List<ErrorCode>) : BaseException(errors)

data class UpdateBeerCommand(
        val beerId: PositiveNumber,
        val name: TextField,
        val description: TextField,
        val beerOwnerId: BeerOwnerId,
        val newNotes: List<NewBeerNote>,
        val notes: List<BeerNote>,
        val newIngredients: List<NewBeerIngredient>,
        val ingredients: List<BeerIngredient>
) : ValidData {

    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                listOf(name, description, beerOwnerId) + newNotes + notes + newIngredients + ingredients
        )
    }

    fun toCreateBeerNoteCommand() = CreateBeerNotes(
            beerId.value,
            newNotes
    )

    fun toCreateBerIngredientsCommand() = CreateBeerIngredientsCommand(
            beerId.value,
            newIngredients
    )

    fun toUpdateBeerIngredientsCommand() = UpdateBeerIngredientsCommand(
            beerId.value,
            ingredients
    )

}

data class BeerUpdatedResult(
        val beerId: Long
)

class BeerUpdateException(errors: List<ErrorCode>) : BaseException(errors)