package beer.diary.api.beer.ingredient

import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import beer.diary.commons.validation.ValidData
import beer.diary.commons.validation.ValidationResult
import beer.diary.commons.validation.ValidationResultFactory

data class Ingredient(
        val id: Long,
        val name: String,
        val info: String
)

data class NewBeerIngredient(
        val name: TextField,
        val info: TextField
) : ValidData {
    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                listOf(name, info)
        )
    }
}

data class BeerIngredient(
        val ingredientId: PositiveNumber,
        val name: TextField,
        val info: TextField
) : ValidData {
    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                listOf(name, info) + ingredientId
        )
    }
}

data class CreateBeerIngredientsCommand(
        val beerId: Long,
        val ingredients: List<NewBeerIngredient>
)

data class UpdateBeerIngredientsCommand(
        val beerId: Long,
        val ingredients: List<BeerIngredient>
)

data class CreateBeerIngredientsResponse(
        val ids: List<Long>
)

data class UpdateBeerIngredientsResponse(
        val ids: List<Long>
)