package beer.diary.api

import beer.diary.api.beer.*
import beer.diary.commons.exception.BaseException
import io.vavr.control.Either

interface BeerApi {
    fun createBeer(createBeerCommand: CreateBeerCommand) : Either<BaseException, BeerCreatedResponse>

    fun readBeer(beerReadCommand: BeerReadCommand): Either<BeerReadException, BeerReadResult>

    fun deleteBeer(beerDeleteCommand: BeerDeleteCommand): Either<BeerDeleteException, BeerDeleteResult>

    fun updateBeer(updateBeerCommand: UpdateBeerCommand): Either<BeerUpdateException, BeerUpdatedResult>
}