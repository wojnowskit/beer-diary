package beer.diary.api.beer.note

import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import beer.diary.commons.validation.ValidData
import beer.diary.commons.validation.ValidationResult
import beer.diary.commons.validation.ValidationResultFactory

data class Note(
        val id: Long,
        val description: String,
        val createdDate: String
) : Comparable<Note> {
    override fun compareTo(other: Note) =
            createdDate.compareTo(other.createdDate)
}

data class NewBeerNote(
        val description: TextField,
        val createdDate: DiaryDate
) : ValidData {
    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                listOf(description, createdDate)
        )
    }
}

data class BeerNote(
        val noteId: PositiveNumber,
        val description: TextField
) : ValidData {
    override fun validate(): ValidationResult {
        return ValidationResultFactory.ofMultiple(
                listOf(description, noteId)
        )
    }
}

data class CreateBeerNotes(
        val beerId: Long,
        val notes: List<NewBeerNote>
)

data class CreatedBeerNoteResponse(
        val noteIds: List<Long>
)