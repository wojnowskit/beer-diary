package beer.diary.conf

import beer.diary.api.*
import beer.diary.api.beer.BeerDeleteCommand
import beer.diary.api.beer.BeerReadCommand
import beer.diary.api.beer.CreateBeerCommand
import beer.diary.api.beer.UpdateBeerCommand
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.beer.create.BeerCreateExecutor
import beer.diary.beer.create.SimpleBeerCreateExecutor
import beer.diary.beer.delete.BeerDeleteExecutor
import beer.diary.beer.delete.SimpleBeerDeleteExecutor
import beer.diary.beer.read.BeerReadExecutor
import beer.diary.beer.read.SimpleBeerReadExecutor
import beer.diary.beer.update.BeerUpdateExecutor
import beer.diary.beer.update.SimpleBeerUpdateExecutor
import beer.diary.datasource.BeerDiaryDbConfiguration
import beer.diary.ingredient.IngredientManager
import beer.diary.search.elastic.api.events.BeerCreatedEvent
import beer.diary.search.elastic.api.events.BeerDeletedEvent
import beer.diary.search.elastic.api.events.BeerUpdatedEvent
import beer.diary.search.elastic.api.events.EventsConsumer

class BeerApiConfiguration(dbConf: BeerDiaryDbConfiguration, private val eventsConsumer: EventsConsumer) {

    private val beerCreateExecutor: BeerCreateExecutor
    private val beerReadExecutor: BeerReadExecutor
    private val beerDeleteExecutor: BeerDeleteExecutor
    private val beerUpdateExecutor: BeerUpdateExecutor
    private val repos = BeerRepositoriesConfig(dbConf.sql2o())
    private val ingredientManager = IngredientManager(repos.ingredientRepository())

    init {
        beerCreateExecutor = SimpleBeerCreateExecutor(
                repos.beerRepository(),
                repos.noteRepository(),
                ingredientManager
        )
        beerReadExecutor = SimpleBeerReadExecutor(repos.beerRepository())
        beerDeleteExecutor = SimpleBeerDeleteExecutor(
                repos.beerRepository(),
                repos.noteRepository(),
                repos.ingredientRepository()
        )
        beerUpdateExecutor = SimpleBeerUpdateExecutor(
                repos.beerRepository(),
                repos.noteRepository(),
                ingredientManager
        )
    }

    fun beerApi(): BeerApi {
        return object : BeerApi {
            override fun createBeer(createBeerCommand: CreateBeerCommand) = beerCreateExecutor.execute(createBeerCommand)
                    .peek {
                        eventsConsumer.consumeCreateEvent(BeerCreatedEvent(
                                it.beerId,
                                createBeerCommand.beerOwnerId.value(),
                                createBeerCommand.name.value(),
                                createBeerCommand.createdDate.date()
                        ))
                    }

            override fun updateBeer(updateBeerCommand: UpdateBeerCommand) = beerUpdateExecutor.execute(updateBeerCommand)
                    .peek {
                        eventsConsumer.consumeUpdateEvent(
                                BeerUpdatedEvent(
                                        updateBeerCommand.beerId.value,
                                        updateBeerCommand.name.value()
                                )
                        )
                    }

            override fun deleteBeer(beerDeleteCommand: BeerDeleteCommand) = beerDeleteExecutor.execute(beerDeleteCommand)
                    .peek {
                        eventsConsumer.consumeDeleteEvent(
                                BeerDeletedEvent(
                                        beerDeleteCommand.beerId.value
                                )
                        )
                    }
            override fun readBeer(beerReadCommand: BeerReadCommand) = beerReadExecutor.execute(beerReadCommand)
        }
    }

}