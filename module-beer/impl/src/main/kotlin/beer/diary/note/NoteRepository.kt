package beer.diary.note

import beer.diary.api.beer.note.BeerNote
import beer.diary.api.beer.note.CreateBeerNotes
import beer.diary.api.beer.note.CreatedBeerNoteResponse
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.datasource.BEER_SCHEMA
import org.sql2o.Sql2o

interface NoteRepository {
    fun deleteNotes(beerId: Long)
    fun createNotes(createBeerNotes: CreateBeerNotes) : CreatedBeerNoteResponse
    fun updateNotes(notes: List<BeerNote>, beerId: Long) : List<Long>
}

class SimpleNoteRepository(private val sql2o: Sql2o) : NoteRepository {

    override fun deleteNotes(beerId: Long) {
        sql2o.open().use {
            it.createQuery("""
                DELETE FROM $BEER_SCHEMA.beer_notes
                WHERE note_beer_id = :beerId
            """.trimIndent())
                    .addParameter("beerId", beerId)
                    .executeUpdate()
        }
    }

    private val insertNoteSql = """
        INSERT INTO $BEER_SCHEMA.beer_notes(note_description, note_created_at, note_beer_id) 
        VALUES(:note_description, :note_created_at, :note_beer_id)
    """.trimIndent()

    private val updateNoteSql = """
        UPDATE $BEER_SCHEMA.beer_notes
        SET 
        note_description = :noteDescription
        WHERE 1=1
        AND note_beer_id = :noteBeerId
        AND note_id = :noteId
    """.trimIndent()

    override fun createNotes(command: CreateBeerNotes): CreatedBeerNoteResponse {
        val savedIds = command.notes.map {
            return@map saveNote(it, command.beerId)
        }
        return CreatedBeerNoteResponse(savedIds)
    }

    override fun updateNotes(notes: List<BeerNote>, beerId: Long) : List<Long> {
        return notes.map {
            return@map updateNote(it, beerId)
        }
    }

    private fun updateNote(note: BeerNote, beerId: Long): Long {
        sql2o.open().use {
            return it.createQuery(updateNoteSql, true)
                    .addParameter("noteDescription", note.description.value())
                    .addParameter("noteBeerId", beerId)
                    .addParameter("noteId", note.noteId.value)
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

    private fun saveNote(note: NewBeerNote, beerId: Long) : Long {
        sql2o.open().use {
            return it.createQuery(insertNoteSql, true)
                    .addParameter("note_description", note.description.value())
                    .addParameter("note_created_at", note.createdDate.date())
                    .addParameter("note_beer_id", beerId)
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

}