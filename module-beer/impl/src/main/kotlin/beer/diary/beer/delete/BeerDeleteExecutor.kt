package beer.diary.beer.delete

import beer.diary.api.beer.BeerDeleteCommand
import beer.diary.api.beer.BeerDeleteException
import beer.diary.api.beer.BeerDeleteResult
import beer.diary.beer.BeerRepository
import beer.diary.commons.executors.MultiValidateExecutor
import beer.diary.ingredient.IngredientRepository
import beer.diary.note.NoteRepository
import io.vavr.control.Either
import org.sql2o.StatementRunnableWithResult

abstract class BeerDeleteExecutor : MultiValidateExecutor<BeerDeleteCommand, BeerDeleteResult, BeerDeleteException>()

class SimpleBeerDeleteExecutor(
        private val beerRepository: BeerRepository,
        private val noteRepository: NoteRepository,
        private val ingredientRepository: IngredientRepository
) : BeerDeleteExecutor() {

    override fun executeValidated(command: BeerDeleteCommand): Either<BeerDeleteException, BeerDeleteResult> {
        return beerRepository.findBeerOwner(command.beerId.value)
                .map { beerOwnerId -> handleDelete(beerOwnerId, command) }
                .orElse(Either.left(BeerDeleteException(emptyList())))
    }

    private fun handleDelete(beerOwnerId: Long, command: BeerDeleteCommand): Either<BeerDeleteException, BeerDeleteResult> {
        if (command.beerOwnerId.notMatch(beerOwnerId))
            return Either.left(BeerDeleteException(emptyList()))
        return beerRepository.sql2o().runInTransaction(StatementRunnableWithResult { _,_ ->
            noteRepository.deleteNotes(command.beerId.value)
            ingredientRepository.deleteIngredients(command.beerId.value)
            beerRepository.deleteBeer(command.beerId.value)
            return@StatementRunnableWithResult Either.right<BeerDeleteException, BeerDeleteResult>(
                    BeerDeleteResult(command.beerId.value)
            )
        })
    }

}