package beer.diary.beer.read

import beer.diary.api.beer.ingredient.Ingredient
import beer.diary.api.beer.note.Note
import beer.diary.commons.fields.FORMATTER
import java.sql.Timestamp

fun List<Map<String, Any>>.beerCreatedBy(): Long {
    return first()["beer_created_by"].toString().toLong()
}

fun List<Map<String, Any>>.beerId(): Long {
    return first()["beer_id"].toString().toLong()
}

fun List<Map<String, Any>>.beerName(): String {
    return first()["beer_name"].toString()
}

fun List<Map<String, Any>>.beerDescription(): String {
    return first()["beer_description"].toString()
}

fun Map<String, Any>.asIngredient(): Ingredient {
    return Ingredient(
            get("beer_ingredient_id").toString().toLong(),
            get("ingredient_name").toString(),
            get("beer_ingredient_info").toString()
    )
}

fun Map<String, Any>.asNote(): Note {
    return Note(
            get("note_id").toString().toLong(),
            get("note_description").toString(),
            (get("note_created_at") as Timestamp).formatDate()
    )
}

fun Timestamp.formatDate(): String {
    return FORMATTER.format(this.toLocalDateTime())
}