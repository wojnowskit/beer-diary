package beer.diary.beer.read

import beer.diary.api.beer.*
import beer.diary.api.beer.ingredient.Ingredient
import beer.diary.api.beer.note.Note
import beer.diary.beer.BeerRepository
import beer.diary.commons.executors.MultiValidateExecutor
import io.vavr.control.Either

abstract class BeerReadExecutor : MultiValidateExecutor<BeerReadCommand, BeerReadResult, BeerReadException>()

class SimpleBeerReadExecutor(private val beerRepository: BeerRepository): BeerReadExecutor() {

    override fun executeValidated(validatedCommand: BeerReadCommand): Either<BeerReadException, BeerReadResult> {
        val beerData = beerRepository.findBeerData(validatedCommand.beerId.value)
        if (beerData.isEmpty())
            return Either.left(BeerReadException(emptyList()))
        val beerResponse = buildBeerResponse(beerData)
        val canReadBeer = canReadBeer(beerData.beerCreatedBy(), validatedCommand.beerOwnerId)
        return if (canReadBeer) Either.right(beerResponse) else Either.left(BeerReadException(emptyList()))
    }

    private fun canReadBeer(beerCreatedById: Long, beerOwnerId: BeerOwnerId) =
            beerOwnerId.match(beerCreatedById)

    private fun buildBeerResponse(beerData: List<Map<String, Any>>) : BeerReadResult {
        val ingredients = mutableSetOf<Ingredient>()
        val notes = mutableSetOf<Note>()
        beerData.forEach {
            ingredients.add(it.asIngredient())
            notes.add(it.asNote())
        }
        return BeerReadResult(
                beerData.beerId(),
                beerData.beerName(),
                beerData.beerDescription(),
                ingredients,
                notes
        )
    }

}
