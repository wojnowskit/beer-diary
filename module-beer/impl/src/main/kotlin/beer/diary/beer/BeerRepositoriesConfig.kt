package beer.diary.beer

import beer.diary.ingredient.SimpleIngredientRepository
import beer.diary.note.SimpleNoteRepository
import org.sql2o.Sql2o

class BeerRepositoriesConfig(sql2o: Sql2o) {
    private val beerRepository = SimpleBeerRepository(sql2o)
    private val ingredientRepository = SimpleIngredientRepository(sql2o)
    private val noteRepository = SimpleNoteRepository(sql2o)

    fun beerRepository() = beerRepository

    fun ingredientRepository() = ingredientRepository

    fun noteRepository() = noteRepository

}