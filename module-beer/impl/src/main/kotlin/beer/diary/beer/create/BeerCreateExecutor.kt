package beer.diary.beer.create

import beer.diary.api.beer.BeerCreatedResponse
import beer.diary.api.beer.CreateBeerCommand
import beer.diary.beer.BeerRepository
import beer.diary.commons.executors.BaseExecutor
import beer.diary.ingredient.IngredientManager
import beer.diary.note.NoteRepository
import org.sql2o.StatementRunnableWithResult


abstract class BeerCreateExecutor : BaseExecutor<CreateBeerCommand, BeerCreatedResponse>()

class SimpleBeerCreateExecutor(private val beerRepository: BeerRepository,
                               private val noteRepository: NoteRepository,
                               private val ingredientManager: IngredientManager
) : BeerCreateExecutor() {

    override fun executeValidated(createBeerCommand: CreateBeerCommand): BeerCreatedResponse {
        return beerRepository.sql2o().runInTransaction(StatementRunnableWithResult { _, _ ->
            val beerItemId = beerRepository.saveBeer(createBeerCommand)
            val notes = noteRepository
                    .createNotes(createBeerCommand.toCreateBeerNoteCommand(beerItemId))
            val ingredients = ingredientManager
                    .save(createBeerCommand.toCreateBerIngredientsCommand(beerItemId))
            return@StatementRunnableWithResult BeerCreatedResponse(
                    beerItemId,
                    notes.noteIds,
                    ingredients.ids
            )
        })
    }

}