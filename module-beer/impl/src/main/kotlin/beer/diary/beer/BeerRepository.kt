package beer.diary.beer

import beer.diary.api.beer.CreateBeerCommand
import beer.diary.api.beer.UpdateBeerCommand
import beer.diary.datasource.BEER_SCHEMA
import org.sql2o.Sql2o
import java.util.*

interface BeerRepository {
    fun sql2o(): Sql2o
    fun saveBeer(createBeerCommand: CreateBeerCommand): Long
    fun updateBeer(updateBeerCommand: UpdateBeerCommand): Long
    fun findBeerOwner(beerId: Long) : Optional<Long>
    fun findBeerData(beerId: Long) : List<Map<String, Any>>
    fun deleteBeer(beerId: Long)
}

class SimpleBeerRepository(private val sql2o: Sql2o) : BeerRepository {

    private val insertBeerSql = """
        INSERT INTO $BEER_SCHEMA.beers(beer_name, beer_description, beer_created_at, beer_created_by) 
        VALUES(:beerName, :beerDescription, :beerCreatedAt, :beerCreatedBy)
    """.trimIndent()

    override fun sql2o() = sql2o

    override fun saveBeer(createBeerCommand: CreateBeerCommand): Long {
        return sql2o.open().use {
            it.createQuery(insertBeerSql, true)
                    .addParameter("beerName", createBeerCommand.name.value())
                    .addParameter("beerDescription", createBeerCommand.description.value())
                    .addParameter("beerCreatedAt", createBeerCommand.createdDate.date())
                    .addParameter("beerCreatedBy", createBeerCommand.beerOwnerId.value())
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

    private val updateBeerSql = """
        UPDATE $BEER_SCHEMA.beers 
        SET
        beer_name = :beerName, 
        beer_description = :beerDescription
        WHERE beer_id = :beerId
    """.trimIndent()

    override fun updateBeer(updateBeerCommand: UpdateBeerCommand): Long {
        return sql2o.open().use {
            it.createQuery(updateBeerSql, true)
                    .addParameter("beerName", updateBeerCommand.name.value())
                    .addParameter("beerDescription", updateBeerCommand.description.value())
                    .addParameter("beerId", updateBeerCommand.beerId.value)
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

    private val findBeerOwnerSql = """
        SELECT b.beer_created_by
        FROM ${BEER_SCHEMA}.beers b
        WHERE 1=1 
        AND beer_id = :beerId
    """.trimIndent()

    override fun findBeerOwner(beerId: Long): Optional<Long> {
        return sql2o.open().use {
            Optional.ofNullable(
                    it.createQuery(findBeerOwnerSql)
                            .addParameter("beerId", beerId)
                            .executeScalar(Long::class.java)
            )
        }
    }

    private val findBeerByIdSql = """
        SELECT 
            b.beer_id, b.beer_name, b.beer_description, b.beer_created_at, b.beer_created_by,
            bn.note_id, bn.note_description, bn.note_created_at,
            bi.beer_ingredient_id, bi.beer_ingredient_info,
            bid.ingredient_name
        FROM ${BEER_SCHEMA}.beers b
        LEFT JOIN ${BEER_SCHEMA}.beer_notes bn ON b.beer_id = bn.note_beer_id
        LEFT JOIN ${BEER_SCHEMA}.beer_ingredients bi ON b.beer_id = bi.beer_ingredient_beer_id
        LEFT JOIN ${BEER_SCHEMA}.ingredients_dict bid ON bi.beer_ingredient_dict_id = bid.ingredient_id
        WHERE 1=1 
        AND beer_id = :beerId
    """.trimIndent()

    override fun findBeerData(beerId: Long): List<Map<String, Any>> {
        return sql2o.open().use {
            it.createQuery(findBeerByIdSql)
                    .addParameter("beerId", beerId)
                    .executeAndFetchTable()
                    .asList()
        }
    }

    override fun deleteBeer(beerId: Long) {
        sql2o.open().use {
            it.createQuery("""
                DELETE FROM ${BEER_SCHEMA}.beers
                WHERE beer_id = :beerId
            """.trimIndent())
                    .addParameter("beerId", beerId)
                    .executeUpdate()
        }
    }

}