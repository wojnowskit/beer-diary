package beer.diary.beer.update

import beer.diary.api.beer.BeerUpdateException
import beer.diary.api.beer.BeerUpdatedResult
import beer.diary.api.beer.UpdateBeerCommand
import beer.diary.beer.BeerRepository
import beer.diary.commons.executors.MultiValidateExecutor
import beer.diary.ingredient.IngredientManager
import beer.diary.note.NoteRepository
import io.vavr.control.Either
import org.sql2o.StatementRunnableWithResult

abstract class BeerUpdateExecutor : MultiValidateExecutor<UpdateBeerCommand, BeerUpdatedResult, BeerUpdateException>()

class SimpleBeerUpdateExecutor(
        private val beerRepository: BeerRepository,
        private val noteRepository: NoteRepository,
        private val ingredientManager: IngredientManager
) : BeerUpdateExecutor() {

    override fun executeValidated(command: UpdateBeerCommand): Either<BeerUpdateException, BeerUpdatedResult> {
        return beerRepository.findBeerOwner(command.beerId.value)
                .map { beerOwnerId -> handleUpdate(beerOwnerId, command) }
                .orElse(Either.left(BeerUpdateException(emptyList())))
    }

    private fun handleUpdate(beerOwnerId: Long, command: UpdateBeerCommand): Either<BeerUpdateException, BeerUpdatedResult> {
        if (command.beerOwnerId.notMatch(beerOwnerId))
            return Either.left(BeerUpdateException(emptyList()))
        return beerRepository.sql2o().runInTransaction(StatementRunnableWithResult { _, _ ->
            beerRepository.updateBeer(command)
            val notes = noteRepository
                    .createNotes(command.toCreateBeerNoteCommand())
            val updatedNotes = noteRepository.updateNotes(command.notes, command.beerId.value)
            val ingredients = ingredientManager
                    .save(command.toCreateBerIngredientsCommand())
            ingredientManager.update(command.toUpdateBeerIngredientsCommand())
            return@StatementRunnableWithResult Either.right<BeerUpdateException, BeerUpdatedResult>(
                    BeerUpdatedResult(command.beerId.value)
            )
        })
    }

}