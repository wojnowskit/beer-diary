package beer.diary.ingredient

import beer.diary.datasource.BEER_SCHEMA
import org.sql2o.Sql2o
import java.util.*

interface IngredientRepository {
    fun deleteIngredients(beerId: Long)
    fun createIngredientDict(ingredientName: String): Long
    fun saveBeerIngredient(ingredientId: Long, beerId: Long, ingredientInfo: String): Long
    fun findIngredientDictIdByName(name: String) : Optional<SimpleIngredientRepository.IdWrapper>
    fun updateIngredient(ingredientUpdate: SimpleIngredientRepository.IngredientUpdate): Long
}

class SimpleIngredientRepository(private val sql2o: Sql2o) : IngredientRepository {

    private val findIngredientDictIdByNameSql = """
        SELECT ingredient_id AS id
        FROM $BEER_SCHEMA.ingredients_dict
        WHERE LOWER(ingredient_name) = LOWER(:ingredientName)
        LIMIT 1
    """.trimIndent()

    private val insertIngredientDictSql = """
        INSERT INTO ${BEER_SCHEMA}.ingredients_dict(ingredient_name) VALUES(:ingredientName)
    """.trimIndent()

    private val insertBeerIngredientSql = """
        INSERT INTO ${BEER_SCHEMA}.beer_ingredients(beer_ingredient_info, beer_ingredient_beer_id, beer_ingredient_dict_id) 
        VALUES(:ingredientInfo, :beerId, :ingredientId)
    """.trimIndent()

    override fun deleteIngredients(beerId: Long) {
        sql2o.open().use {
            it.createQuery("""
                DELETE FROM $BEER_SCHEMA.beer_ingredients
                WHERE beer_ingredient_beer_id = :beerId
            """.trimIndent())
                    .addParameter("beerId", beerId)
                    .executeUpdate()
        }
    }

    override fun createIngredientDict(ingredientName: String): Long {
        return sql2o.open().use {
            it.createQuery(insertIngredientDictSql, true)
                    .addParameter("ingredientName", ingredientName)
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

    override fun saveBeerIngredient(ingredientId: Long, beerId: Long, ingredientInfo: String): Long {
        return sql2o.open().use {
            it.createQuery(insertBeerIngredientSql, true)
                    .addParameter("ingredientInfo", ingredientInfo)
                    .addParameter("beerId", beerId)
                    .addParameter("ingredientId", ingredientId)
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

    override fun findIngredientDictIdByName(name: String) : Optional<IdWrapper> {
        return Optional.ofNullable(sql2o.open().use {
            it.createQuery(findIngredientDictIdByNameSql)
                    .addParameter("ingredientName", name)
                    .executeAndFetchFirst(IdWrapper::class.java)
        })
    }

    private val updateBeerIngredientSql = """
        UPDATE ${BEER_SCHEMA}.beer_ingredients
        SET 
        beer_ingredient_info = :info, 
        beer_ingredient_dict_id = :ingredientDictId 
        WHERE 1=1
        AND beer_ingredient_beer_id = :beerId
        AND beer_ingredient_id = :ingredientId
    """.trimIndent()

    override fun updateIngredient(ingredientUpdate: IngredientUpdate): Long {
        return sql2o.open().use {
            it.createQuery(updateBeerIngredientSql, true)
                    .addParameter("info", ingredientUpdate.info)
                    .addParameter("ingredientDictId", ingredientUpdate.nameDictId)
                    .addParameter("beerId", ingredientUpdate.beerId)
                    .addParameter("ingredientId", ingredientUpdate.id)
                    .executeUpdate()
                    .key.toString().toLong()
        }
    }

    data class IdWrapper(
            val id: Long
    )

    data class IngredientUpdate(
            val id:Long,
            val beerId:Long,
            val nameDictId:Long,
            val info: String
    )

}