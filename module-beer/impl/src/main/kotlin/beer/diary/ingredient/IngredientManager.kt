package beer.diary.ingredient

import beer.diary.api.beer.ingredient.*
import beer.diary.ingredient.SimpleIngredientRepository.IngredientUpdate

class IngredientManager(private val ingredientRepository:IngredientRepository) {

    fun save(createBeerIngredientsCommand: CreateBeerIngredientsCommand): CreateBeerIngredientsResponse {
        val ingredientIds = createBeerIngredientsCommand.ingredients.map {
            saveOrCreate(it, createBeerIngredientsCommand.beerId)
        }
        return CreateBeerIngredientsResponse(ingredientIds)
    }

    private fun saveOrCreate(ingredient: NewBeerIngredient, beerId: Long): Long {
        return ingredientRepository.findIngredientDictIdByName(ingredient.name.value())
                .map {
                    ingredientRepository.saveBeerIngredient(it.id, beerId, ingredient.info.value())
                }.orElseGet {
                    var id: Long = ingredientRepository.createIngredientDict(ingredient.name.value())
                    ingredientRepository.saveBeerIngredient(id, beerId, ingredient.info.value())
                }
    }

    fun update(command: UpdateBeerIngredientsCommand) : UpdateBeerIngredientsResponse {
        val ingredientIds = command.ingredients
                .map { createUpdateData(it, command.beerId) }
                .map { ingredientRepository.updateIngredient(it) }
        return UpdateBeerIngredientsResponse(ingredientIds)
    }

    private fun createUpdateData(ingredient: BeerIngredient, beerId: Long): IngredientUpdate {
        val ingredientNameDictId = handleIngredientDictName(ingredient.name.value())
        return IngredientUpdate(
                ingredient.ingredientId.value,
                beerId,
                ingredientNameDictId,
                ingredient.info.value()
        )
    }

    private fun handleIngredientDictName(ingredientName: String) : Long {
        return ingredientRepository.findIngredientDictIdByName(ingredientName)
                .map { it.id }
                .orElseGet {ingredientRepository.createIngredientDict(ingredientName)}
    }

}