package beer.diary.base

import beer.diary.commons.fields.FieldsKt
import beer.diary.datasource.BeerDiaryDbConfiguration
import beer.diary.datasource.UnitTestBeerDiaryDbConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import groovy.sql.Sql
import org.mindrot.jbcrypt.BCrypt
import spock.lang.Shared
import spock.lang.Specification

import java.text.SimpleDateFormat

import static beer.diary.datasource.BeerDiaryDbConfigurationKt.BEER_SCHEMA

class BaseSpecification extends Specification {

	ObjectMapper mapper = new ObjectMapper()

	@Shared
	BeerDiaryDbConfiguration dbConf = new UnitTestBeerDiaryDbConfiguration()
	@Shared
	Sql sql = new Sql(dbConf.dataSource())

	def setup() {
		clearDbData()
	}

	def cleanup() {
		clearDbData()
	}

	def addHashedUser(username, rawPassword) {
		addUser(username, BCrypt.hashpw(rawPassword, BCrypt.gensalt()))
	}

	def addHashedUser(username) {
		addHashedUser(username, username)
	}

	def clearDbData() {
		def clearTables = [
		        "app_users",
				"beer_notes",
				"beer_ingredients",
				"ingredients_dict",
				"beers"
		]
		clearTables.each {
			sql.execute("DELETE FROM ${BEER_SCHEMA}.${it}".toString())
		}
	}

	def addUser(username, password) {
		sql.executeInsert(
				"INSERT INTO ${BEER_SCHEMA}.app_users(user_name, user_password) VALUES('${username}', '${password}') RETURNING user_id".toString()
		).get(0).get(0) as Long
	}
	
	def addBeer(beerName, beerDesc, beerCreatedAt, beerCreatedBy) {
		sql.executeInsert(
			"""
			INSERT INTO ${BEER_SCHEMA}.beers(beer_name, beer_description, beer_created_at, beer_created_by) 
			VALUES('${beerName}','${beerDesc}','${beerCreatedAt}',${beerCreatedBy}) RETURNING beer_id
			""".toString()
		).get(0).get(0) as Long
	}
	
	def addIngredient(ingredientName) {
		sql.executeInsert(
				"INSERT INTO ${BEER_SCHEMA}.ingredients_dict(ingredient_name) VALUES('${ingredientName}') RETURNING ingredient_id".toString()
		).get(0).get(0) as Long
	}
	
	def countTable(tableName) {
		sql.firstRow("SELECT COUNT(*) AS count FROM ${BEER_SCHEMA}.${tableName}".toString())
			.get("count") as Long
	}
	
	def fetchAllFromTable(tableName) {
		sql.rows("SELECT * FROM ${BEER_SCHEMA}.${tableName}".toString())
	}
	
	def fetchFirstFromTable(tableName) {
		fetchAllFromTable(tableName).first()
	}
	
	def fetchAllFromTableNoId(tableName) {
		sql.rows("SELECT * FROM ${BEER_SCHEMA}.${tableName}".toString())
		.collect {
			it.findAll {
				!it.key.toString().contains("id")
			}
		}
	}
	
	def fetchAllFromTableNoColumn(tableName, column) {
		sql.rows("SELECT * FROM ${BEER_SCHEMA}.${tableName}".toString())
				.collect {
					it.findAll {
						!it.key.toString().equals(column)
					}
				}
	}
	
	def fetchFirstFromTableNoId(tableName) {
		fetchAllFromTableNoId(tableName).first()
	}

	def removeFromObject(Map data, String key) {
		def newData = data.collectEntries {it}
		newData.remove(key)
		newData
	}

	def toJson(value) {
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
		mapper.setDateFormat(new SimpleDateFormat(FieldsKt.DATE_FORMAT))
		mapper.writeValueAsString(value)
	}

	def findDeep(Map m, String key) {
		if (m.containsKey(key)) return m[key]
		m.findResult { k, v -> v instanceof Map ? findDeep(v, key) : null }
	}

	def notContainsId(str) {
		!str.toString().toLowerCase().contains("id")
	}

	def toMapNoIds(value) {
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
		mapper.setDateFormat(new SimpleDateFormat(FieldsKt.DATE_FORMAT))
		def jsonWithIds = mapper.writeValueAsString(value)
		def mapWithIds = mapper.readValue(jsonWithIds, Map.class)
		def result = [:]
		mapWithIds.keySet().findAll { notContainsId(it) }.sort().forEach {
			def val = mapWithIds[it]
			if (val instanceof List) {
				handleListItem(it.toString(), val, result)
			} else {
				result[it] = val
			}
		}
		return result
	}

	void handleListItem(String key, List list, Map<Object, Object> result) {
		def newList = []
		list.each {
			def map = it as Map
			newList.add(
				map.findAll { notContainsId(it) }.sort()
			)
		}
		result[key] = newList
	}
}
