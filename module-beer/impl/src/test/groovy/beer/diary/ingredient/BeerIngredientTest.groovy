package beer.diary.ingredient

import beer.diary.api.beer.ingredient.BeerIngredient
import beer.diary.api.beer.ingredient.CreateBeerIngredientsCommand
import beer.diary.api.beer.ingredient.NewBeerIngredient
import beer.diary.api.beer.ingredient.UpdateBeerIngredientsCommand
import beer.diary.base.BaseSpecification
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import spock.lang.Subject
import spock.lang.Unroll

class BeerIngredientTest extends BaseSpecification {

	def repoConfig = new BeerRepositoriesConfig(dbConf.sql2o())

	@Subject
	IngredientManager ingredientManager

	def setup() {
		ingredientManager = new IngredientManager(repoConfig.ingredientRepository())
	}

	def "should create ingredients"() {
		given:
			def date = new DiaryDate("2020-11-14 20:45").date()
			def beerId = addBeer("test beer", "test desc", date, 1)
			def req = new CreateBeerIngredientsCommand(beerId,
													   [new NewBeerIngredient(new TextField("name", "test name1"),
																			  new TextField("info", "test info1")),
														new NewBeerIngredient(new TextField("name", "test name2"),
																			  new TextField("info", "test info2"))])
		when:
			def response = ingredientManager.save(req)
		then:
			response.ids.size() == 2
			fetchAllFromTableNoId("ingredients_dict").flatten {it["ingredient_name"]}.containsAll("test name1", "test name2")
			fetchAllFromTableNoId("beer_ingredients").
				flatten {it["beer_ingredient_info"]}.
				containsAll("test info1", "test info2")
	}

	def "should create ingredient from existing"() {
		given:
			def existingIngredient = addIngredient("Test Ingredient")
			def date = new DiaryDate("2020-11-14 20:45").date()
			def beerId = addBeer("test beer", "test desc", date, 1)
			def req = new CreateBeerIngredientsCommand(beerId,
													   [new NewBeerIngredient(new TextField("name", "test ingredient"),
																			  new TextField("info", "test info1"))])
		when:
			def response = ingredientManager.save(req)
		then:
			response.ids.size() == 1
			fetchAllFromTableNoId("ingredients_dict").flatten {it["ingredient_name"]}.containsAll("Test Ingredient")
			fetchAllFromTableNoId("beer_ingredients").flatten {it["beer_ingredient_info"]}.containsAll("test info1")
			fetchFirstFromTable("beer_ingredients")["beer_ingredient_dict_id"] == existingIngredient
	}

	@Unroll
	def "should update ingredients"() {
		given: "two ingredients"
			addIngredient("Ingredient one")
			addIngredient("Ingredient two")
		and: "create ingredient"
			def beerId = addBeer()
			def ingredient = addNewIngredient(beerId, "Ingredient one", "Ingredient info")
		and:
			def updateCmd = updateCommand(beerId, ingredient, ingredientName, ingredientInfo)
		when:
			def updateRes = ingredientManager.update(updateCmd)
		then:
			"$expectedAssertion"()
			!updateRes.ids.isEmpty()
		where:
			ingredientName     | ingredientInfo             || expectedAssertion
			"Ingredient one"   | "Ingredient info - edited" || "assert01"
			"Ingredient one"   | "Ingredient info"          || "assert02"
			"Ingredient two"   | "Ingredient info"          || "assert03"
			"Ingredient three" | "Ingredient info"          || "assert04"
	}

	void assert01() {
		assert countTable("ingredients_dict") == 2
		assert fetchFirstFromTableNoId("beer_ingredients") == ["beer_ingredient_info": "Ingredient info - edited"]
	}

	void assert02() {
		assert countTable("ingredients_dict") == 2
		assert fetchFirstFromTableNoId("beer_ingredients") == ["beer_ingredient_info": "Ingredient info"]
	}

	void assert03() {
		assert countTable("ingredients_dict") == 2
		assert fetchFirstFromTableNoId("beer_ingredients") == ["beer_ingredient_info": "Ingredient info"]
		def ingredient = fetchFirstFromTable("beer_ingredients")
		def dictId = fetchAllFromTable("ingredients_dict").find {it["ingredient_name"] == "Ingredient two"}
		assert ingredient["beer_ingredient_info"] == "Ingredient info"
		assert ingredient["beer_ingredient_dict_id"] == dictId["ingredient_id"]
	}

	void assert04() {
		assert countTable("ingredients_dict") == 3
		assert fetchFirstFromTableNoId("beer_ingredients") == ["beer_ingredient_info": "Ingredient info"]
		def ingredient = fetchFirstFromTable("beer_ingredients")
		def dictId = fetchAllFromTable("ingredients_dict").find {it["ingredient_name"] == "Ingredient three"}
		assert ingredient["beer_ingredient_info"] == "Ingredient info"
		assert ingredient["beer_ingredient_dict_id"] == dictId["ingredient_id"]
	}

	def addBeer() {
		def date = new DiaryDate("2020-11-14 20:45").date()
		addBeer("test beer", "test desc", date, 1)
	}

	def updateCommand(beerId, ingredientId, name, info) {
		new UpdateBeerIngredientsCommand(beerId, [new BeerIngredient(new PositiveNumber(ingredientId),
																	 new TextField("name", name),
																	 new TextField("info", info))])
	}

	def addNewIngredient(beerId, name, info) {
		def cmd = new CreateBeerIngredientsCommand(beerId,
												   [new NewBeerIngredient(new TextField("name", name),
																		  new TextField("info", info))])
		ingredientManager.save(cmd).ids.first()
	}

}
