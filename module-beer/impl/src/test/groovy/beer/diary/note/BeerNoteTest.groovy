package beer.diary.note

import beer.diary.api.beer.note.CreateBeerNotes
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.base.BaseSpecification
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.TextField
import spock.lang.Subject

class BeerNoteTest extends BaseSpecification {

	def repoConfig = new BeerRepositoriesConfig(dbConf.sql2o())

	@Subject
	NoteRepository noteRepository

	def setup() {
		noteRepository = repoConfig.noteRepository()
	}
	
	def "should add beer note"() {
		given:
			def date = new DiaryDate("2020-11-14 20:45").date()
			def addedBeer = addBeer("test beer", "test desc", date, 1)
			def command = new CreateBeerNotes(
					addedBeer,
					[
							new NewBeerNote(
									new TextField("desc", "test opis1"),
									DiaryDate.@Companion.of("2020-01-14 12:54")
							),
							new NewBeerNote(
									new TextField("desc", "test opis2"),
									DiaryDate.@Companion.of("2020-01-14 12:55")
							)
					]
			)
		when:
			def executeResponse = noteRepository.createNotes(command)
			def dbData = fetchAllFromTable("beer_notes").sort { it["note_created_at"] }
		then:
			dbData.size() == executeResponse.noteIds.size()
			executeResponse.noteIds.containsAll(dbData.collect { it["note_id"] as Long })
			dbData[0]["note_beer_id"] == addedBeer
			dbData[0]["note_description"] == "test opis1"
			dbData[0]["note_created_at"].toString() == "2020-01-14 12:54:00.0"
			dbData[1]["note_beer_id"] == addedBeer
			dbData[1]["note_description"] == "test opis2"
			dbData[1]["note_created_at"].toString() == "2020-01-14 12:55:00.0"
	}
	
	def "should not add beer note" () {
		given:
			def date = new DiaryDate("2020-11-14 20:45").date()
			def addedBeer = addBeer("test beer", "test desc", date, 1)
			def command = new CreateBeerNotes(
					addedBeer,
					[
							new NewBeerNote(
									new TextField("desc", "test opis1"),
									DiaryDate.@Companion.of("")
							)
					]
			)
		when:
			noteRepository.createNotes(command)
		then:
			thrown(Exception)
			countTable("beer_notes") == 0
	}

}
