package beer.diary.beer.read

import beer.diary.api.beer.BeerOwnerId
import beer.diary.api.beer.BeerReadCommand
import beer.diary.base.BaseSpecification
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.beer.create.BeerCreateExecutor
import beer.diary.beer.create.SimpleBeerCreateExecutor
import beer.diary.commons.fields.PositiveNumber
import beer.diary.ingredient.IngredientManager
import beer.diary.utils.TestData
import spock.lang.Subject

class BeerReadExecutorTest extends BaseSpecification {

	def repoConfig = new BeerRepositoriesConfig(dbConf.sql2o())
	BeerCreateExecutor beerCreateExecutor

	@Subject
	BeerReadExecutor beerReadExecutor

	def setup() {
		beerCreateExecutor = new SimpleBeerCreateExecutor(
			repoConfig.beerRepository(),
			repoConfig.noteRepository(),
			new IngredientManager(repoConfig.ingredientRepository())
		)
		beerReadExecutor = new SimpleBeerReadExecutor(repoConfig.beerRepository())
	}

	def "should read added beer" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			def createdBeerId = beerCreateExecutor.execute(cmd).get().beerId
		when:
			def readRequest = new BeerReadCommand(
				new PositiveNumber(createdBeerId),
				cmd.beerOwnerId
			)
			def readResponse = beerReadExecutor.execute(readRequest)
		then:
			readResponse.isRight()
			toMapNoIds(readResponse.get()) == [
				"description":"test desc",
				"ingredients":[
					["info":"dummy info", "name":"dummy name"]
				],
				"name":"test beer",
				"notes":[
					["createdDate":"2020-06-12 12:13", "description":"dummy note"],
					["createdDate":"2020-06-12 12:15", "description":"dummy note"]
				]
			]

	}

	def "should not read not existing beer" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			beerCreateExecutor.execute(cmd).get().beerId
		when:
			def readRequest = new BeerReadCommand(
				new PositiveNumber(23456),
				cmd.beerOwnerId
			)
			def readResponse = beerReadExecutor.execute(readRequest)
		then:
			readResponse.isLeft()
	}

	def "should not read beer when owner is wrong" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			def createdBeerId = beerCreateExecutor.execute(cmd).get().beerId
		when:
			def readRequest = new BeerReadCommand(
				new PositiveNumber(createdBeerId),
				new BeerOwnerId(new PositiveNumber(12345L))
			)
			def readResponse = beerReadExecutor.execute(readRequest)
		then:
			readResponse.isLeft()
	}

}
