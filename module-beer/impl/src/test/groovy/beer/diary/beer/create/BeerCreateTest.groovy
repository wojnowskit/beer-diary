package beer.diary.beer.create

import beer.diary.api.beer.BeerOwnerId
import beer.diary.api.beer.CreateBeerCommand
import beer.diary.api.beer.ingredient.NewBeerIngredient
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.base.BaseSpecification
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.commons.exception.BaseException
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import beer.diary.ingredient.IngredientManager
import beer.diary.utils.TestData
import spock.lang.Subject

class BeerCreateTest extends BaseSpecification {

    def repoConfig = new BeerRepositoriesConfig(dbConf.sql2o())
    
    @Subject
    BeerCreateExecutor beerCreateExecutor
    
    def setup() {
        beerCreateExecutor = new SimpleBeerCreateExecutor(
            repoConfig.beerRepository(),
            repoConfig.noteRepository(),
            new IngredientManager(repoConfig.ingredientRepository())
        )
    }
    
    def "should create beer"() {
        given:
            assert countTable("beers") == 0
            def cmd = TestData.dummyCreateBeerCmd()
        when:
            def res = beerCreateExecutor.execute(cmd).get()
        then:
            res.beerId > 0
        and:
            countTable("beers") == 1
            toJson(fetchFirstFromTableNoId("beers")) == toJson([
                "beer_name":"test beer",
                "beer_description":"test desc",
                "beer_created_at":"2020-12-16 20:20",
                "beer_created_by":1
            ])
            countTable("beer_notes") == 2
            countTable("ingredients_dict") == 1
            countTable("beer_ingredients") == 1
    }
    
    def "should return exception when create beer"() {
        given:
            def cmd = new CreateBeerCommand(
                new TextField("beerName", ""),
                new TextField("beerDesc", ""),
                new BeerOwnerId(new PositiveNumber(1L)),
                new DiaryDate("2020-12-16 20:20"),
                [
                            new NewBeerNote(
                                    new TextField("noteDesc", ""),
                                    new DiaryDate("2020-06-12 12:13")
                            )
                    ],
                [
                            new NewBeerIngredient(
                                    new TextField("name", "dummy name"),
                                    new TextField("info", "dummy info")
                            )
                    ]
            )
        when:
            def res = beerCreateExecutor.execute(cmd).left
        then:
            res.class == BaseException.class
            res.info()["errors"].size() == 3
    }
    
}
