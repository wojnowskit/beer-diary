package beer.diary.beer.update

import beer.diary.api.beer.BeerOwnerId
import beer.diary.api.beer.BeerReadCommand
import beer.diary.api.beer.UpdateBeerCommand
import beer.diary.api.beer.ingredient.BeerIngredient
import beer.diary.api.beer.ingredient.NewBeerIngredient
import beer.diary.api.beer.note.BeerNote
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.base.BaseSpecification
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.beer.create.BeerCreateExecutor
import beer.diary.beer.create.SimpleBeerCreateExecutor
import beer.diary.beer.read.BeerReadExecutor
import beer.diary.beer.read.SimpleBeerReadExecutor
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import beer.diary.ingredient.IngredientManager
import beer.diary.utils.TestData
import spock.lang.Subject

class BeerUpdateExecutorTest extends BaseSpecification {

	def repoConfig = new BeerRepositoriesConfig(dbConf.sql2o())
	BeerCreateExecutor beerCreateExecutor
	BeerReadExecutor beerReadExecutor

	@Subject
	BeerUpdateExecutor beerUpdateExecutor

	def setup() {
		def ingredientManager = new IngredientManager(repoConfig.ingredientRepository())
		beerCreateExecutor = new SimpleBeerCreateExecutor(
			repoConfig.beerRepository(),
			repoConfig.noteRepository(),
			ingredientManager
		)
		beerReadExecutor = new SimpleBeerReadExecutor(
			repoConfig.beerRepository()
		)
		beerUpdateExecutor = new SimpleBeerUpdateExecutor(
			repoConfig.beerRepository(),
			repoConfig.noteRepository(),
			ingredientManager
		)
	}

	def "should update added beer" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			def createdBeerId = beerCreateExecutor.execute(cmd).get().beerId
			def readCommand = new BeerReadCommand(new PositiveNumber(createdBeerId),
												  cmd.beerOwnerId)
			def beer = beerReadExecutor.execute(readCommand).get()
			def noteId = beer.notes.first().id
			def ingredient = beer.ingredients.first()
		when:
			def command = new UpdateBeerCommand(
				new PositiveNumber(createdBeerId),
				new TextField("name","dummy name edited"),
				new TextField("desc","dummy desc edited"),
				cmd.beerOwnerId,
				[
					new NewBeerNote(
						new TextField("desc", "dummy note created with update"),
						new DiaryDate("2020-06-13 13:12")
					)
				],
				[
				    new BeerNote(
						new PositiveNumber(noteId),
						new TextField("desc", "i am existing item edited")
					)
				],
				[
				    new NewBeerIngredient(
						new TextField("name", "dummy ingredient created with update"),
						new TextField("info", "dummy ingredient created with update - info")
					)
				],
				[
					new BeerIngredient(
						new PositiveNumber(ingredient.id),
						new TextField("name", ingredient.name + " - edited also"),
						new TextField("info", ingredient.info)
					)
				]
			)
			def result = beerUpdateExecutor.execute(command)
		then:
			result.isRight()
			countTable("beers") == 1
			toJson(fetchFirstFromTableNoId("beers")) == toJson([
				"beer_name":"dummy name edited",
				"beer_description":"dummy desc edited",
				"beer_created_at":"2020-12-16 20:20",
				"beer_created_by":1
			])
			countTable("beer_notes") == 3
			fetchAllFromTable("beer_notes").find {
				it["note_description"] == "dummy note created with update"
			}
			fetchAllFromTable("beer_notes").find {
				it["note_description"] == "i am existing item edited"
			}
			fetchAllFromTable("beer_ingredients").find {
				it["beer_ingredient_info"] == "dummy ingredient created with update - info"
			}
			fetchAllFromTable("ingredients_dict").find {
				it["ingredient_name"] == "dummy name - edited also"
			}
	}

	def "should not update not existing beer" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			beerCreateExecutor.execute(cmd).get().beerId
		when:
			def command = new UpdateBeerCommand(
				new PositiveNumber(123456),
				new TextField("name","dummy name"),
				new TextField("desc","dummy desc"),
				cmd.beerOwnerId,
				[],
				[],
				[],
				[]
			)
			def result = beerUpdateExecutor.execute(command)
		then:
			result.isLeft()
	}

	def "should not update beer when owner is wrong" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			def createdBeerId = beerCreateExecutor.execute(cmd).get().beerId
		when:
			def command = new UpdateBeerCommand(
				new PositiveNumber(createdBeerId),
				new TextField("name","dummy name"),
				new TextField("desc","dummy desc"),
				new BeerOwnerId(new PositiveNumber(12345L)),
				[],
				[],
				[],
				[]
			)
			def result = beerUpdateExecutor.execute(command)
		then:
			result.isLeft()
	}

}
