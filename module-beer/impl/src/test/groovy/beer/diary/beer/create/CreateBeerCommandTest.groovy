package beer.diary.beer.create

import beer.diary.api.beer.BeerOwnerId
import beer.diary.api.beer.CreateBeerCommand
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField
import spock.lang.Specification
import spock.lang.Unroll

class CreateBeerCommandTest extends Specification {

	@Unroll
	def "should validate CreateBeerCommand"() {
		given:
			def cmd = new CreateBeerCommand(
				new TextField("beerName", beerName),
				new TextField("description", "desc"),
				new BeerOwnerId(new PositiveNumber(1L)),
				new DiaryDate("2020-12-16 20:20"),
				[],
				[]
			)
		when:
			def validate = cmd.validate()
		then:
			cmd.name.value() == expectedName
			validate.isValid() == expectedValid
		where:
			beerName || expectedName || expectedValid
			" sss "  || "sss"        || true
			""       || ""           || false
			"    "   || ""           || false
	}

}
