package beer.diary.beer.delete

import beer.diary.api.beer.BeerDeleteCommand
import beer.diary.api.beer.BeerOwnerId
import beer.diary.base.BaseSpecification
import beer.diary.beer.BeerRepositoriesConfig
import beer.diary.beer.create.BeerCreateExecutor
import beer.diary.beer.create.SimpleBeerCreateExecutor
import beer.diary.commons.fields.PositiveNumber
import beer.diary.ingredient.IngredientManager
import beer.diary.utils.TestData
import spock.lang.Subject

class BeerDeleteExecutorTest extends BaseSpecification {

	def repoConfig = new BeerRepositoriesConfig(dbConf.sql2o())
	BeerCreateExecutor beerCreateExecutor

	@Subject
	BeerDeleteExecutor beerDeleteExecutor

	def setup() {
		beerCreateExecutor = new SimpleBeerCreateExecutor(
			repoConfig.beerRepository(),
			repoConfig.noteRepository(),
			new IngredientManager(repoConfig.ingredientRepository())
		)
		beerDeleteExecutor = new SimpleBeerDeleteExecutor(
			repoConfig.beerRepository(),
			repoConfig.noteRepository(),
			repoConfig.ingredientRepository()
		)
	}

	def "should delete added beer" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			def createdBeerId = beerCreateExecutor.execute(cmd).get().beerId
		and:
			def deleteCommand = new BeerDeleteCommand(
				new PositiveNumber(createdBeerId),
				cmd.beerOwnerId
			)
		when:
			def result = beerDeleteExecutor.execute(deleteCommand)
		then:
			result.isRight()
			result.get().beerId == createdBeerId
			countTable("beers") == 0
			countTable("beer_notes") == 0
			countTable("beer_ingredients") == 0
	}

	def "should not delete beer when owner is wrong" () {
		given:
			def cmd = TestData.dummyCreateBeerCmd()
			def createdBeerId = beerCreateExecutor.execute(cmd).get().beerId
		and:
			def deleteCommand = new BeerDeleteCommand(
				new PositiveNumber(createdBeerId),
				new BeerOwnerId(new PositiveNumber(12345L))
			)
		when:
			def result = beerDeleteExecutor.execute(deleteCommand)
		then:
			result.isLeft()
	}

}
