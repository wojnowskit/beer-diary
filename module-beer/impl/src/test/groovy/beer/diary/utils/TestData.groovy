package beer.diary.utils

import beer.diary.api.beer.BeerOwnerId
import beer.diary.api.beer.CreateBeerCommand
import beer.diary.api.beer.ingredient.NewBeerIngredient
import beer.diary.api.beer.note.NewBeerNote
import beer.diary.commons.fields.DiaryDate
import beer.diary.commons.fields.PositiveNumber
import beer.diary.commons.fields.TextField

class TestData {

	static dummyCreateBeerCmd() {
		new CreateBeerCommand(
			new TextField("", "test beer"),
			new TextField("", "test desc"),
			new BeerOwnerId(new PositiveNumber(1L)),
			new DiaryDate("2020-12-16 20:20"),
			[
				new NewBeerNote(
					new TextField("desc", "dummy note"),
					new DiaryDate("2020-06-12 12:13")
				),
				new NewBeerNote(
					new TextField("desc", "dummy note"),
					new DiaryDate("2020-06-12 12:15")
				)
			],
			[
				new NewBeerIngredient(
					new TextField("name", "dummy name"),
					new TextField("info", "dummy info")
				)
			]
		)
	}

}
